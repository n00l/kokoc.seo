<?php
defined("B_PROLOG_INCLUDED") and (B_PROLOG_INCLUDED === true) or die();

$MESS["SEO_ENTITY_FIELD_ID"] = "ID";
$MESS["SEO_ENTITY_FIELD_ACTIVE"] = "Активен";
$MESS["SEO_ENTITY_FIELD_URL"] = "Старый url";
$MESS["SEO_ENTITY_FIELD_NEW_URL"] = "Новый url";
$MESS["SEO_ENTITY_FIELD_ADD_PARAMS"] = "Добавляемые параметры";
$MESS["SEO_ENTITY_FIELD_META_ID"] = "META";