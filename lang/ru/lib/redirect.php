<?php
defined("B_PROLOG_INCLUDED") and (B_PROLOG_INCLUDED === true) or die();

$MESS["SEO_ENTITY_FIELD_ID"] = "ID";
$MESS["SEO_ENTITY_FIELD_ACTIVE"] = "Активен";
$MESS["SEO_ENTITY_FIELD_URL"] = "Старый url";
$MESS["SEO_ENTITY_FIELD_NEW_URL"] = "Новый url";
$MESS["SEO_ENTITY_FIELD_TYPE"] = "Тип";
$MESS["SEO_ENTITY_FIELD_IB_ID"] = "ID эл./сек.";
$MESS["SEO_ENTITY_FIELD_IB_XML_ID"] = "Внешний ID эл./сек.";
$MESS["SEO_ENTITY_FIELD_IB_PAR_ID"] = "ID родителя";
$MESS["SEO_ENTITY_FIELD_COUNT"] = "Использований";
$MESS["SEO_ENTITY_FIELD_LAST_USE"] = "Последнее";