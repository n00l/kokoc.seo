<?php
defined("B_PROLOG_INCLUDED") and (B_PROLOG_INCLUDED === true) or die();

$MESS["KS_CACHE_TAB"] = "Кэш";
$MESS["KS_CACHE_BUT"] = "Очистить кэш модуля";

$MESS["KS_MODULE_ACTIVE"] = "Активен";
$MESS["KS_REDIRECTS"] = "Редиректы";
$MESS["KS_REDIRECTS_ACTIVE"] = "Использовать редиректы";
$MESS["KS_REDIRECTS_TRACK_ACTIVE"] = "Отслеживать изменения в инфоблоках:";
$MESS["KS_META"] = "META";
$MESS["KS_META_ACTIVE"] = "Применять мета тэги";
$MESS["KS_META_FORCE"] = "Use the Force Luke";
$MESS["KS_SEF"] = "SEF";
$MESS["KS_SEF_ACTIVE"] = "Использовать короткие ссылки";
$MESS["KS_OTHER"] = "Другое";
$MESS["KS_TRANSLIT_ACTIVE"] = "Использовать транслит по правилам Яндекс";
$MESS["KS_PAGINATION_ACTIVE"] = "Использовать шаблоны для страниц пагинации";
$MESS["KS_PAGINATION_TEMPLATES"] = "Шаблоны: ";
$MESS["KS_CACHE_TTL"] = "Время жизни: ";

$MESS["REFERENCES_OPTIONS_SAVED"] = "Настройки сохранены";
$MESS["REFERENCES_INVALID_VALUE"] = "Введено неверное значение";
