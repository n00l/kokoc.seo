<?php
defined("B_PROLOG_INCLUDED") and (B_PROLOG_INCLUDED === true) or die();

$MESS["PAGEN_TEMPLATE_H1"] = "#H1# - страница №#PAGE#";
$MESS["PAGEN_TEMPLATE_TITLE"] = "#TITLE# - страница №#PAGE#";
$MESS["PAGEN_TEMPLATE_DESCR"] = "#DESCR# - страница №#PAGE#";
$MESS["PAGEN_TEMPLATE_KEYWR"] = "#KEYWR#";