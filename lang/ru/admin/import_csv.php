<?php
$MESS['KOKOC_SEO_ERR_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['KOKOC_SEO_ERR_TABLE_NOT_SET'] = 'Не задана таблица для импорта';
$MESS['KOKOC_SEO_ERR_INVALID_TABLE'] = 'Таблица не существует';
$MESS['KOKOC_SEO_ERR_INVALID_FILE_EXT'] = 'Не верный тип файла';
$MESS['KOKOC_SEO_ERR_CANT_READ_FILE'] = 'Не могу прочитать файл';
$MESS['KOKOC_SEO_ERR_INVALID_FILE_HEARER'] = 'Не валидный заголовок';
$MESS['KOKOC_SEO_TAB_TEXT'] = 'Доступные поля';
$MESS['KOKOC_SEO_IMPORT_SUC'] = 'Успешно импортировано: ';
$MESS['KOKOC_SEO_IMPORT_SUBMIT'] = 'Импортировать';