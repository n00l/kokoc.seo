<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['KOKOC_SEO_SEFS_OFF'] = 'Использование отключено';
$MESS['KOKOC_ADDEL_TITLE'] = 'Добавить новый редирект';
$MESS['KOKOC_ADDEL_TEXT'] = 'Новый';
$MESS['KOKOC_IMPORT_TITLE'] = 'Импортировать из csv файла';
$MESS['KOKOC_IMPORT_TEXT'] = 'Импорт';
$MESS['KOKOC_LIST_ACTIVATE'] = 'Активировать';
$MESS['KOKOC_LIST_DEACTIVATE'] = 'Деактивировать';

$MESS['HEAD_ACTIVE'] = 'Активен';
$MESS['HEAD_URL'] = 'Старый url';
$MESS['HEAD_NEW_URL'] = 'Новый url';
$MESS['HEAD_ADD_PARAMS'] = 'Добавляемые параметры';
$MESS['HEAD_META_ID'] = 'META';

$MESS['FILTER_H_ID'] = 'ID';
$MESS['FILTER_H_META_ID'] = 'META';
$MESS['FILTER_H_ACTIVE'] = 'Статус';
$MESS['FILTER_H_URL'] = 'Старая ссылка';
$MESS['FILTER_H_NEW_URL'] = 'Новая ссылка';
$MESS['FILTER_ID'] = 'ID';
$MESS['FILTER_ID_FROM'] = 'от';
$MESS['FILTER_ID_TO'] = 'до';
$MESS['FILTER_URL'] = 'Старая ссылка';
$MESS['FILTER_NEW_URL'] = 'Новая ссылка';
$MESS['FILTER_ACTIVE'] = 'Статусу';
$MESS['FILTER_ACTIVE_ALL'] = 'все';
$MESS['FILTER_ACTIVE_YES'] = 'активные';
$MESS['FILTER_ACTIVE_NO'] = 'не активные';
$MESS['FILTER_META_ID'] = 'META ID';