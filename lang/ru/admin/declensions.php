<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['KOKOC_SEO_META_OFF'] = 'Использование отключено';
$MESS['KOKOC_ADDEL_TITLE'] = 'Добавить новое правило';
$MESS['KOKOC_ADDEL_TEXT'] = 'Новый';
$MESS['KOKOC_IMPORT_TITLE'] = 'Импортировать из csv файла';
$MESS['KOKOC_IMPORT_TEXT'] = 'Импорт';
$MESS['KOKOC_SEO_TO_MANUAL'] = 'проверено';

$MESS['HEAD_TYPE'] = 'Тип';
$MESS['HEAD_TARGET'] = 'Цель';
$MESS['HEAD_GENDER'] = 'Пол';
$MESS['HEAD_CASE'] = 'Падеж';
$MESS['HEAD_NUMBER'] = 'Число';
$MESS['HEAD_VALUE'] = 'Значение';
$MESS['HEAD_GENERATE'] = 'Создано';

$MESS['FILTER_H_ID'] = 'ID';
$MESS['FILTER_H_TYPE'] = 'Тип';
$MESS['FILTER_H_TARGET'] = 'Цель';
$MESS['FILTER_H_GENDER'] = 'Пол';
$MESS['FILTER_H_CASE'] = 'Падеж';
$MESS['FILTER_H_NUMBER'] = 'Число';
$MESS['FILTER_H_GENERATE'] = 'Создано';
$MESS['FILTER_ID'] = 'ID';
$MESS['FILTER_ID_FROM'] = 'от';
$MESS['FILTER_ID_TO'] = 'до';
$MESS['FILTER_TYPE'] = 'Тип';
$MESS['FILTER_TARGET'] = 'Цель';
$MESS['FILTER_GENDER'] = 'Пол';
$MESS['FILTER_NUMBER'] = 'Число';
$MESS['FILTER_CASE'] = 'Падеж';
$MESS['FILTER_GENERATE'] = 'Создано';
$MESS['FILTER_ALL'] = 'все';

$MESS['GENDER_A'] = 'авто';
$MESS['GENDER_M'] = 'муж.';
$MESS['GENDER_W'] = 'жен.';
$MESS['GENDER_N'] = 'ср.';
$MESS['NUMBER_S'] = 'ед.ч.';
$MESS['NUMBER_P'] = 'мн.ч.';
$MESS['GENERATE_A'] = 'авто';
$MESS['GENERATE_M'] = 'руч';