<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['SEO_E_SAVE'] = 'Не могу загрузить модуль';
$MESS['SEO_T_TAB1'] = 'Параметры';
$MESS['SEO_T_TAB1_T'] = 'Параметры';
$MESS['SEO_T_LIST'] = 'Вернуться';
$MESS['SEO_T_LIST_TITLE'] = 'Вернуться';
$MESS['SEO_T_CREATE'] = 'Новый';
$MESS['SEO_T_CREATE_TITLE'] = 'Новый';
$MESS['SEO_T_DEL'] = 'Удалить';
$MESS['SEO_T_DEL_TITLE'] = 'Удалить';

$MESS['SEO_T_ID'] = 'ID';
$MESS['SEO_T_TYPE'] = 'Тип';
$MESS['SEO_T_TARGET'] = 'Цель';
$MESS['SEO_T_GENDER'] = 'Пол';
$MESS['SEO_T_CASE'] = 'Падеж';
$MESS['SEO_T_NUMBER'] = 'Число';
$MESS['SEO_T_VALUE'] = 'Значение';

$MESS['SEO_T_GENDER_A'] = 'авто';
$MESS['SEO_T_GENDER_M'] = 'муж.';
$MESS['SEO_T_GENDER_W'] = 'жен.';
$MESS['SEO_T_GENDER_N'] = 'ср.';
$MESS['SEO_T_NUMBER_S'] = 'ед.ч.';
$MESS['SEO_T_NUMBER_P'] = 'мн.ч.';