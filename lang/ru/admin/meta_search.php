<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['KOKOC_SEO_META_OFF'] = 'Использование отключено';
$MESS['KOKOC_ADDEL_TITLE'] = 'Добавить новое правило';
$MESS['KOKOC_ADDEL_TEXT'] = 'Новый';
$MESS['KOKOC_LIST_ACTIVATE'] = 'Активировать';
$MESS['KOKOC_LIST_DEACTIVATE'] = 'Деактивировать';
$MESS['KOKOC_META_SELECT'] = 'Выбрать';

$MESS['HEAD_ACTIVE'] = 'Активен';
$MESS['HEAD_URL'] = 'url';
$MESS['HEAD_H1'] = 'h1';
$MESS['HEAD_TITLE'] = 'title';
$MESS['HEAD_DESCRIPTION'] = 'description';
$MESS['HEAD_KEYWORDS'] = 'keywords';
$MESS['HEAD_CANONICAL'] = 'canonical';
$MESS['HEAD_TEXT_TOP'] = 'Текст t';
$MESS['HEAD_TEXT_MIDDLE'] = 'Текст m';
$MESS['HEAD_TEXT_BOTTOM'] = 'Текст b';
$MESS['TEXT_EMPTY'] = 'нет';
$MESS['TEXT_N_EMPTY'] = 'есть';

$MESS['FILTER_H_ID'] = 'ID';
$MESS['FILTER_H_ACTIVE'] = 'Статус';
$MESS['FILTER_H_URL'] = 'Старая ссылка';
$MESS['FILTER_H_CANONICAL'] = 'Кононикл';
$MESS['FILTER_ID'] = 'ID';
$MESS['FILTER_ID_FROM'] = 'от';
$MESS['FILTER_ID_TO'] = 'до';
$MESS['FILTER_URL'] = 'Ссылка';
$MESS['FILTER_ACTIVE'] = 'Статусу';
$MESS['FILTER_ACTIVE_ALL'] = 'все';
$MESS['FILTER_ACTIVE_YES'] = 'активные';
$MESS['FILTER_ACTIVE_NO'] = 'не активные';
$MESS['FILTER_CANONICAL'] = 'Кононикл';