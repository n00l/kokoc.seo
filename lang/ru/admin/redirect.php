<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['SEO_E_SAVE'] = 'Не могу загрузить модуль';
$MESS['SEO_T_TAB1'] = 'Параметры';
$MESS['SEO_T_TAB1_T'] = 'Параметры';
$MESS['SEO_T_LIST'] = 'Вернуться';
$MESS['SEO_T_LIST_TITLE'] = 'Вернуться';
$MESS['SEO_T_CREATE'] = 'Новый';
$MESS['SEO_T_CREATE_TITLE'] = 'Новый';
$MESS['SEO_T_DEL'] = 'Удалить';
$MESS['SEO_T_DEL_TITLE'] = 'Удалить';

$MESS['SEO_T_ID'] = 'ID';
$MESS['SEO_T_ACTIVE'] = 'Активен';
$MESS['SEO_T_URL'] = 'Старая ссылка';
$MESS['SEO_T_NEW_URL'] = 'Новая ссылка';
$MESS['SEO_T_TYPE'] = 'Тип';
$MESS['SEO_T_TYPE_D'] = 'обычный';
$MESS['SEO_T_TYPE_E'] = 'елемент';
$MESS['SEO_T_TYPE_S'] = 'раздел';
$MESS['SEO_T_TYPE_X'] = 'выражение';
$MESS['SEO_T_IB_ID'] = 'ID эл./сек.';
$MESS['SEO_T_IB_XML_ID'] = 'Внешний ID эл./сек.';
$MESS['SEO_T_IB_PAR_ID'] = 'ID родителя';
$MESS['SEO_T_COUNT'] = 'Использований';
$MESS['SEO_T_LAST_USE'] = 'Последнее использование';