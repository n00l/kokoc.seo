<?php
$MESS['KOKOC_SEO_MODULE_CANT_LOAD'] = 'Не могу загрузить модуль';
$MESS['SEO_E_SAVE'] = 'Не могу загрузить модуль';
$MESS['SEO_T_TAB1'] = 'Параметры';
$MESS['SEO_T_TAB1_T'] = 'Параметры';
$MESS['SEO_T_LIST'] = 'Вернуться';
$MESS['SEO_T_LIST_TITLE'] = 'Вернуться';
$MESS['SEO_T_CREATE'] = 'Новый';
$MESS['SEO_T_CREATE_TITLE'] = 'Новый';
$MESS['SEO_T_DEL'] = 'Удалить';
$MESS['SEO_T_DEL_TITLE'] = 'Удалить';

$MESS['SEO_T_ID'] = 'ID';
$MESS['SEO_T_ACTIVE'] = 'Активен';
$MESS['SEO_T_URL'] = 'Старая ссылка';
$MESS['SEO_T_NEW_URL'] = 'Новая ссылка';
$MESS['SEO_T_ADD_PARAMS'] = 'Добавляемые GET параметры';
$MESS['SEO_T_META_ID'] = 'META ID';
$MESS['SEO_T_META_FIND'] = 'Выбрать';
$MESS['SEO_T_META_ADD'] = 'Добавить';
$MESS['SEO_T_META_H1'] = 'h1';
$MESS['SEO_T_META_TITLE'] = 'title';
$MESS['SEO_T_META_DESCRIPTION'] = 'description';
$MESS['SEO_T_META_KEYWORDS'] = 'keywords';
$MESS['SEO_T_META_TT'] = 'Верхний текст';
$MESS['SEO_T_META_TM'] = 'Средний текст';
$MESS['SEO_T_META_TB'] = 'Нижний текст';