# Модуль Kokoc Seo
Для чего нужен:
1. Редиректы
2. META тэги страниц
3. Короткие ссылки
4. Генерация текстов со склонением по падежам
5. Пагинация
и д.р.

Подробнее:
https://docs.google.com/document/d/1DRvErffQ5Q67Phx9vR4Gn6uZV7WS9Z92bh6RYPwBk74/edit?usp=sharing



# Установка и настройка

Скачиваем, кладём в local.
Рабочий стол > Marketplace > Установленные решения.
Устанавливаем:
Рабочий стол > Настройки > Настройки продукта > Настройки модулей > Kokoc SEO.
Выбираем нужные настройки:

Подробнее про настройки далее.

Прим. Требуется версия битры 17.
Прим2. Почти не тестировалось.



# Редиректы

Если в настройках стоит флажок то проверяется таблица редиректов, админка:
Рабочий стол > Маркетинг > Kokoc SEO > Редиректы

Типы: обычный - простое перенаправление со страницы на страницу;
Элемент - указывается старый url и id и/или xml_id элемента. Перенаправляет на url по настройкам инфоблока. Если id/xml_id не указан/не найден перенаправит на страницу раздела-родителя (если тот задан и найден);
Раздел - тоже самое;
Выражение - регулярка.

API:
```php?start_inline=1
$arFields = array(
    'ACTIVE' => 'N', // по умолчанию Y
    'URL' => '/catalog/razdel/element/',
    'NEW_URL' => '/product/element/',
    'TYPE' => 'E', // 'D', 'E', 'S', 'X' -> обычный, элемент, раздел, регулярка
    'IB_ID' => '', // ID элемента / раздела
    'IB_XML_ID' => 'product', // XML_ID элемента / раздела
    'IB_PAR_ID' => '1', // ID родительского раздела
);
Kokoc\Seo\RedirectTable::add($arFields);
Kokoc\Seo\RedirectTable::getList([
    'filter' => ['URL' => $url],
    'select' => ['ID','ACTIVE','URL','NEW_URL','TYPE','IB_ID','IB_XML_ID','IB_PAR_ID'],
    'cache' => ['ttl' => 86400]
]);
Kokoc\Seo\RedirectTable::delete($id);
```
и т.д.



# SEF

Если в настройках стоит флажок то проверяется таблица sef, админка:
Рабочий стол > Маркетинг > Kokoc SEO > Правила url

Используется для задания коротких ссылок.
При заходе на старую ссылку будет перенаправление на новую.
При заходе на новую - чпу будет обрабатываться как для старой.
Можно сразу выбрать META из существующих, или задать новые:

```php?start_inline=1
Kokoc\Seo\SefTable
$arFields = array(
    'ACTIVE' => 'N', // по умолчанию Y
    'URL' => '/catalog/razdel/3581456/', // не пустое
    'NEW_URL' => '/product/tovar/', // уникальное
    'ADD_PARAMS' => 'id=3581456&section=razdel', // добавляется в массив $_GET
    'META_ID' => '1' // id в таблице META
);
```
При выборке getList доступно поле 'META’ - left join из мета таблицы по 'META_ID'.



# META

Если в настройках стоит флажок то проверяется таблица meta, админка:
Рабочий стол > Маркетинг > Kokoc SEO > META тэги
```php?start_inline=1
Kokoc\Seo\MetaTable
$arFields = array(
    'ACTIVE' => 'N', // по умолчанию Y
    'URL' => '',
    'H1' => '',
    'TITLE' => '',
    'DESCRIPTION' => '',
    'KEYWORDS' => '',
    'CANONICAL' => '',
    'TEXT_TOP' => '',
    'TEXT_TOP_TYPE' => '', // 'TEXT', 'HTML', по умолчанию 'TEXT'
    'TEXT_MIDDLE' => '',
    'TEXT_MIDDLE_TYPE' => '',
    'TEXT_BOTTOM' => '',
    'TEXT_BOTTOM_TYPE' => '',
);
```

Кроме API таблицы доступны методы:
```php?start_inline=1
Kokoc\Seo::setMeta('h1', $arResult[‘NAME’]); // имя, значение
Kokoc\Seo::setMeta(array('text_footer' => 'my text', 'h1' => 'заголовок')); // или массив
$APPLICATION->AddBufferContent(array('Kokoc\Seo', 'getSeoText'), 'footer'); // вывод
```

# Склонения слов
Склонение текста по падежам, числам, полам.

API:
```php?start_inline=1
Kokoc\Seo::getCase(
    $value, // текст
    $case, // 'им','рд','дт','вн','тв','пр' с возможностью генерации, либо выдуманный, который ищется только в таблице
    $number = null, // 'ед','мн', по умолчанию единственный
    $gender = null, // 'муж','жен','ср', по умолчанию A - auto попытка предсказать род по существительным
    $type = null, // подсказка где используется, например 'for text generation sections'
    $generate = true, // (bool) использовать ли автогенерацию, или только искать в таблице
    $save = false, // (bool) сохранять ли автогенерацию в таблице
    &$manual // (bool) в переменную устанавливается флаг при выборке из таблицы. false - сгенерировано, true - ручное задание или проверенное
);
Изменить поведение по умолчанию можно так:
\Bitrix\Main\Config\Option::set('kokoc.seo', 'generate_declensions', false);
\Bitrix\Main\Config\Option::set('kokoc.seo', 'save_declensions', true);

Метод работы  только с авто генерацией:
getWordsCase(
    $value,
    $case = false, // 'МН','ЕД'
    $number = false, // 'ИМ','РД','ДТ','ВН','ТВ','ПР'
    $gender = false // 'МР', 'ЖР', 'СР'
);
```
Кеширование и работа с таблицей им не производятся.
Админка: Рабочий стол > Маркетинг > Kokoc SEO > Склонения



# Другие настройки и возможности


Есть также возможность включить ЧПУ пагинацию установив опцию
```php?start_inline=1
\Bitrix\Main\Config\Option::set('kokoc.seo', 'use_sef_pagination', 'Y');
/catalog/section/page-5/
```
Страницы с /page-#/ на конце будут обрабатываться как без оного, а номер страницы будет добавлен в пагинации.



# Примеры
```php?start_inline=1
// новое ЧПУ товаров Яндекс транслитом с редиректами
if (\Bitrix\Main\Loader::includeModule("iblock")
    && \Bitrix\Main\Loader::includeModule("kokoc.seo")
) {
    $el = new CIBlockElement;
    $res = CIBlockElement::GetList(
        Array("SORT"=>"ASC"),
        Array("IBLOCK_TYPE" => "catalog"),
        false,
        false,
        Array("ID", "IBLOCK_ID", "XML_ID", "SECTION_ID", "NAME", "CODE", "DETAIL_PAGE_URL")
    );
    while ($arElement = $res->GetNext()){
        $new_code = Kokoc\Seo::translit($arElement["NAME"]);
        if ($arElement["CODE"] != $new_code) {
            $el->Update($arElement["ID"], array("CODE" => $new_code));
            Kokoc\Seo\RedirectTable::add(array(
                "URL" => $arElement["DETAIL_PAGE_URL"],
                "TYPE" => "E",
                "IB_ID" => $arElement["ID"],
                "IB_XML_ID" => $arElement["XML_ID"],
                "IB_PAR_ID" => $arElement["SECTION_ID"],
            ));
        }
    }
    Kokoc\Seo\RedirectTable::getEntity()->cleanCache();
}
```

##  Генерация META  в каталоге

```php?start_inline=1
// файл result_modifier.php
$arResult["META"]['H1'] = $arResult['SECTION']['NAME'].' '.$arResult['NAME'];
$arResult["META"]['TITLE'] = $arResult['SECTION']['NAME'].' '.$arResult['NAME']
    .' купить коллекцию '.$arResult['NAME'].' по выгодным ценам в Москве';
$arResult["META"]['DESCRIPTION'] =     'Купить коллекцию '
    .mb_strtolower(Kokoc\Seo::getCase($arResult['SECTION']['NAME'], 'рд', 'ед', null, 'for collection', null, true))
    .' '.$arResult['NAME'].' в интернет-магазине'
    .($arResult['MIN_PRICE']['VALUE']?' по цене от '.$arResult['MIN_PRICE']['VALUE'].' руб.':'')
    .' с доставкой по Москве и МО. Звоните: 8 (925) 706-45-89';
$arResult["META"]['TEXT_FOOTER'] = 'В магазине вы можете купить коллекцию '
    .mb_strtolower(Kokoc\Seo::getCase($arResult['SECTION']['NAME'], 'рд', 'ед', null, 'for collection', null, true))
    .' '.$arResult['NAME'].($arResult['MIN_PRICE']['VALUE']?' по цене от '.$arResult['MIN_PRICE']['VALUE'].' руб.':'')
    .', бренда '.$arResult["PROPERTIES"]["FACTORY"]["VALUE"]
    .'. В каталоге представлены фото коллекции '.$arResult['NAME']
    .' в интерьере. Также при возникновении вопросов, вы можете позвонить по телефону: 8 (925) 706-45-89'
    .', либо заказать обратный звонок на сайте.';

$this->getComponent()->SetResultCacheKeys(array('META'));
// файл component_epilog.php
if (!empty($arResult["META"]))
    Kokoc\Seo::setMeta($arResult["META"]);


## Фильтр со ссылками
```php?start_inline=1
// файл result_modifier.php
$url = str_replace('/filter/clear/apply/','/filter/#SMART_FILTER_PATH#/apply/'
    ,$arResult["JS_FILTER_PARAMS"]["SEF_DEL_FILTER_URL"]);
foreach($arResult["ITEMS"] as $PID => $arItem){
    if ($arItem["PRICE"] || $arItem["PROPERTY_TYPE"] == "N"    || $arItem["DISPLAY_TYPE"] == "U")
        continue;

    foreach($arItem["VALUES"] as $key => $ar){
        if (!$ar["CHECKED"] && !$ar["DISABLED"]) {
            $link = $this->GetComponent()->makeSmartUrl($url, true, $ar["CONTROL_ID"]);
            $arLinks[$link] = '';
            $arResult["ITEMS"][$PID]["VALUES"][$key]['LINK'] = $link;
        }
    }
}
if ((CModule::IncludeModule("kokoc.seo")
    && (Bitrix\Main\Config\Option::get("kokoc.seo", "use_sef_spoof") == "Y")
    && class_exists("\Kokoc\Seo\SefTable"))
) {
    $query = \Kokoc\Seo\SefTable::getList(array(
        'select' => array('URL', 'NEW_URL'),
        'filter' => array('URL' => array_keys($arLinks)),
        'cache' => array('ttl' => 86400)
    ));
    $arLinks = array();
    while ($result = $query->fetch()){
        $arLinks[$result['URL']] = $result['NEW_URL'];
    }
    foreach($arResult["ITEMS"] as $PID => $arItem){
        foreach($arItem["VALUES"] as $key => $ar){
            if ($ar["LINK"] && array_key_exists($ar["LINK"], $arLinks)) {
                $arResult["ITEMS"][$PID]["VALUES"][$key]['LINK'] = $arLinks[$ar["LINK"]];
            }
        }
    }
    $query = \Kokoc\Seo\SefTable::getList(array(
        'select' => array('URL', 'NEW_URL'),
        'filter' => array('URL' => $arResult["SEF_SET_FILTER_URL"]),
        'cache' => array('ttl' => 3600)
    ));
    if ($result = $query->fetch()){
        $arResult["FILTER_URL"] = $arResult["SEF_SET_FILTER_URL"] = $result['NEW_URL'];
    }
}
```