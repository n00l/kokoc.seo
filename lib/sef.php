<?php
namespace Kokoc\Seo;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class SefTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'kkc_seo_sef';
	}

	public static function getMap()
	{
		return array(
			'ID' => new Entity\IntegerField('ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ID'),
				'autocomplete' => true,
				'primary' => true,
			)),
			'ACTIVE' => new Entity\BooleanField('ACTIVE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ACTIVE'),
				'values' => array('N', 'Y'),
				'default_value' => 'Y',
			)),
			'URL' => new Entity\StringField('URL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_URL'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(1, 255),
					);
				},
			)),
			'NEW_URL' => new Entity\StringField('NEW_URL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_NEW_URL'),
				'validation' => function() {
					return array(
						new Entity\Validator\Unique()
					);
				},
			)),
			'ADD_PARAMS' => new Entity\TextField('ADD_PARAMS', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ADD_PARAMS'),
			)),
			'META_ID' => new Entity\IntegerField('META_ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_META_ID'),
			)),
			'META' => new Entity\ReferenceField(
				'META',
				'Kokoc\Seo\Meta',
				array('=this.META_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
		);
	}
}