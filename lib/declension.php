<?php
namespace Kokoc\Seo;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class DeclensionTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'kkc_seo_property_declension';
	}

	public static function getMap()
	{
		return array(
			'ID' => new Entity\IntegerField('ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ID'),
				'autocomplete' => true,
				'primary' => true,
			)),
			'TARGET' => new Entity\StringField('TARGET', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TARGET'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(1, 255),
					);
				},
			)),
			'VALUE' => new Entity\StringField('VALUE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_VALUE'),
				'validation' => function() {
					return array(
						new Entity\Validator\Length(1, 255),
					);
				},
			)),
			'CASE' => new Entity\StringField('CASE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_CASE'),
				'validation' => function() {
					return array(
						new Entity\Validator\Length(1, 255),
					);
				},
			)),
			'GENDER' => new Entity\EnumField('GENDER', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_GENDER'),
				'values' => array('A', 'M', 'W', 'N'),
				'default_value' => 'A',
			)),
			'NUMBER' => new Entity\EnumField('NUMBER', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_NUMBER'),
				'values' => array('S', 'P'),
				'default_value' => 'S',
			)),
			'TYPE' => new Entity\StringField('TYPE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TYPE'),
				'validation' => function() {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'GENERATE' => new Entity\EnumField('GENERATE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_GENERATE'),
				'values' => array('A', 'M'),
				'default_value' => 'A',
			)),
		);
	}
}