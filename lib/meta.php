<?php
namespace Kokoc\Seo;

use Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class MetaTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'kkc_seo_meta';
	}

	public static function getMap()
	{
		return array(
			'ID' => new Entity\IntegerField('ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ID'),
				'autocomplete' => true,
				'primary' => true,
			)),
			'ACTIVE' => new Entity\BooleanField('ACTIVE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ACTIVE'),
				'values' => array('N', 'Y'),
				'default_value' => 'Y',
			)),
			'URL' => new Entity\StringField('URL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_URL'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'H1' => new Entity\StringField('H1', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_H1'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'TITLE' => new Entity\StringField('TITLE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TITLE'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'DESCRIPTION' => new Entity\StringField('DESCRIPTION', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_DESCRIPTION'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'KEYWORDS' => new Entity\StringField('KEYWORDS', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_KEYWORDS'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'CANONICAL' => new Entity\StringField('CANONICAL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_CANONICAL'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'TEXT_TOP' => new Entity\TextField('TEXT_TOP', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_TOP'),
			)),
			'TEXT_TOP_TYPE' => new Entity\EnumField('TEXT_TOP_TYPE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_TOP_TYPE'),
				'values' => array('TEXT', 'HTML'),
				'default_value' => 'TEXT',
			)),
			'TEXT_MIDDLE' => new Entity\TextField('TEXT_MIDDLE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_MIDDLE'),
			)),
			'TEXT_MIDDLE_TYPE' => new Entity\EnumField('TEXT_MIDDLE_TYPE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_MIDDLE_TYPE'),
				'values' => array('TEXT', 'HTML'),
				'default_value' => 'TEXT',
			)),
			'TEXT_BOTTOM' => new Entity\TextField('TEXT_BOTTOM', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_BOTTOM'),
			)),
			'TEXT_BOTTOM_TYPE' => new Entity\EnumField('TEXT_BOTTOM_TYPE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TEXT_BOTTOM_TYPE'),
				'values' => array('TEXT', 'HTML'),
				'default_value' => 'TEXT',
			)),
		);
	}
}