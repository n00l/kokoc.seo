<?php
namespace Kokoc\Seo;

use Bitrix\Main,
	Bitrix\Main\Type,
	Bitrix\Main\Entity,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class RedirectTable extends Entity\DataManager
{
	public static function getTableName()
	{
		return 'kkc_seo_redirects';
	}

	public static function getMap()
	{
		return array(
			'ID' => new Entity\IntegerField('ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ID'),
				'autocomplete' => true,
				'primary' => true,
			)),
			'ACTIVE' => new Entity\BooleanField('ACTIVE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_ACTIVE'),
				'values' => array('N', 'Y'),
				'default_value' => 'Y',
			)),
			'URL' => new Entity\StringField('URL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_URL'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(1, 255),
					);
				},
			)),
			'NEW_URL' => new Entity\StringField('NEW_URL', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_NEW_URL'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'TYPE' => new Entity\EnumField('TYPE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_TYPE'),
				'values' => array('D', 'E', 'S', 'X'),
				'default_value' => 'D',
			)),
			'IB_ID' => new Entity\StringField('IB_ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_IB_ID'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'IB_XML_ID' => new Entity\StringField('IB_XML_ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_IB_XML_ID'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'IB_PAR_ID' => new Entity\StringField('IB_PAR_ID', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_IB_PAR_ID'),
				'validation' => function () {
					return array(
						new Entity\Validator\Length(null, 255),
					);
				},
			)),
			'COUNT' => new Entity\IntegerField('COUNT', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_COUNT'),
			)),
			'LAST_USE' => new Entity\DatetimeField('LAST_USE', array(
				'title' => Loc::getMessage('SEO_ENTITY_FIELD_LAST_USE'),
				'fetch_data_modification' => function () {
					return array(
						function ($value) {
							if (is_a($value, 'Bitrix\Main\Type\DateTime'))
								return $value->toString();
							return $value;
						}
					);
				},
				'save_data_modification' => function () {
					return array(
						function ($value) {
							if (is_string($value))
								return new Type\DateTime($value);
							return $value;
						}
					);
				}
			)),
		);
	}
}
