<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Kokoc\Seo;

Loc::loadMessages(__FILE__);

class kokoc_seo extends CModule
{
	public function __construct()
	{
		$arModuleVersion = array();
		
		include __DIR__ . '/version.php';

		if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}
		
		$this->MODULE_ID = 'kokoc.seo';
		$this->MODULE_NAME = Loc::getMessage('KOKOC_SEO_MODULE_NAME');
		$this->MODULE_DESCRIPTION = Loc::getMessage('KOKOC_SEO_MODULE_DESCRIPTION');
		$this->MODULE_GROUP_RIGHTS = 'N';
		$this->PARTNER_NAME = Loc::getMessage('KOKOC_SEO_MODULE_PARTNER_NAME');
		$this->PARTNER_URI = 'http://kokoc.com/';
	}

	public function doInstall()
	{
		global $APPLICATION;
		if (!IsModuleInstalled($this->MODULE_ID))
		{
			ModuleManager::registerModule($this->MODULE_ID);
			$this->installFiles();
			$this->installDB();
			$this->registerEvents();
			$APPLICATION->IncludeAdminFile(Loc::getMessage('KOKOC_SEO_MODULE_INSTALL'), __DIR__ . "/step.php");
		}
	}

	public function doUninstall()
	{
		global $APPLICATION;
		$this->uninstallFiles();
		$this->uninstallDB();
		$this->unregisterEvents();
		ModuleManager::unRegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile(Loc::getMessage('KOKOC_SEO_MODULE_UNINSTALL'), __DIR__ . "/unstep.php");
	}

	public function installFiles()
	{
		CopyDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true, true);
	}

	public function installDB()
	{
		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			if (!$connection->isTableExists(Seo\MetaTable::getTableName()))
			{
				Seo\MetaTable::getEntity()->createDbTable();
			}
			if (!$connection->isTableExists(Seo\RedirectTable::getTableName()))
			{
				Seo\RedirectTable::getEntity()->createDbTable();
			}
			if (!$connection->isTableExists(Seo\SefTable::getTableName()))
			{
				Seo\SefTable::getEntity()->createDbTable();
			}
			if (!$connection->isTableExists(Seo\DeclensionTable::getTableName()))
			{
				Seo\DeclensionTable::getEntity()->createDbTable();
			}
		}
	}

	public function registerEvents()
	{
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->registerEventHandler('main', 'OnPanelCreate', $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnPanelCreate");
		$eventManager->registerEventHandler("main", "OnPageStart", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnPageStart");
		$eventManager->registerEventHandler("main", "OnProlog", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnProlog");
		$eventManager->registerEventHandler("main", "OnEpilog", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnEpilog");
	}

	public function uninstallFiles()
	{
		DeleteDirFiles(__DIR__ . "/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
	}

	public function uninstallDB()
	{
		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			if ($connection->isTableExists(Seo\MetaTable::getTableName()))
			{
				$connection->dropTable(Seo\MetaTable::getTableName());
			}
			if ($connection->isTableExists(Seo\RedirectTable::getTableName()))
			{
				$connection->dropTable(Seo\RedirectTable::getTableName());
			}
			if ($connection->isTableExists(Seo\SefTable::getTableName()))
			{
				$connection->dropTable(Seo\SefTable::getTableName());
			}
			if ($connection->isTableExists(Seo\DeclensionTable::getTableName()))
			{
				$connection->dropTable(Seo\DeclensionTable::getTableName());
			}
		}
	}

	public function unregisterEvents()
	{
		$eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->unRegisterEventHandler('main', 'OnPanelCreate', $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnPanelCreate");
		$eventManager->unRegisterEventHandler("main", "OnPageStart", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnPageStart");
		$eventManager->unRegisterEventHandler("main", "OnProlog", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnProlog");
		$eventManager->unRegisterEventHandler("main", "OnEpilog", $this->MODULE_ID, "\\Kokoc\\Seo\\EventHandlers", "OnEpilog");

		\CAgent::RemoveModuleAgents($this->MODULE_ID);
	}
}
