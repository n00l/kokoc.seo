<?php
\Bitrix\Main\Loader::registerAutoLoadClasses(
	'kokoc.seo',
	array(
		'kokoc_seo' => 'install/index.php',
		'\Kokoc\Seo' => 'classes/general/seo.php',
		'\Kokoc\Seo\EventHandlers' => 'classes/general/eventhandlers.php',
	)
);