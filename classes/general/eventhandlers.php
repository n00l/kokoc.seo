<?php
namespace Kokoc\Seo;

use CSite,
	CHTTP,
	CJSCore,
	CIBlock,
	CIBlockElement,
	CIBlockSection,
	Bitrix\Main,
	Bitrix\Main\DB,
	Bitrix\Main\Loader,
	Bitrix\Main\Context,
	Bitrix\Main\HttpRequest,
	Bitrix\Main\Application,
	Bitrix\Main\EventManager,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Type\DateTime,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo,
	Kokoc\Seo\SefTable as ST,
	Kokoc\Seo\MetaTable as MT,
	Kokoc\Seo\RedirectTable as RT;

Loc::loadMessages(__FILE__);

class EventHandlers
{
	const MODULE_ID = 'kokoc.seo';

	private static $arIBlocks = array();
	private static $arIBlockElements = array();
	private static $arIBlockSections = array();

	public static function OnPageStart()
	{
		if (Option::get(self::MODULE_ID, 'module_active') != 'Y'){
			return;
		}
		if (Option::get(self::MODULE_ID, 'use_redirects') == 'Y'){
			self::checkRedirect();
		}
		if (true || !empty(Option::get(self::MODULE_ID, 'redirect_track_ib', ''))){
			self::setTrackIB();
		}
		if (Option::get(self::MODULE_ID, 'use_sef_spoof') == 'Y'){
			self::checkSEF();
		}
		if (Option::get(self::MODULE_ID, 'use_sef_pagination') == 'Y'){
			self::paginationSEF();
		}

		return;
	}

	public static function OnProlog()
	{
		if (Option::get(self::MODULE_ID, 'module_active') != 'Y'){
			return;
		}
		if (Option::get(self::MODULE_ID, 'change_admin_translit') == 'Y'){
			self::JSCoreRegisterTranslit();
		}

		return;
	}

	public static function OnEpilog()
	{
		if (Option::get(self::MODULE_ID, 'module_active') != 'Y'){
			return;
		}
		if (Option::get(self::MODULE_ID, 'meta_active') == 'Y'){
			self::checkMeta();
		}
		self::setMataTags();
		if (Option::get(self::MODULE_ID, 'meta_force_rewrite') == 'Y'){
			EventManager::getInstance()->AddEventHandler('main', 'OnEndBufferContent', Array('Kokoc\Seo\EventHandlers', 'forceChangeMeta')); 
		}
		if (Option::get(self::MODULE_ID, 'seo_pagination_active') == 'Y'){
			self::paginationAddMeta();
		}

		return;
	}

	public static function OnPanelCreate()
	{
		// for future button add
		return;
	}

	public static function checkRedirect()
	{
		if (defined('ADMIN_SECTION')) return;
		$aInstance = Application::getInstance();
		$context = $aInstance->getContext();
		$request = $context->getRequest();
		$url = $request->getRequestedPageDirectory().'/';
		$uri = $request->getRequestUri();

		$cache = $aInstance->getCache();
		$cacheTtl = Option::get(self::MODULE_ID, 'cache_ttl', 3600);
		$path = 'kokoc.seo/redirects';
		if ($cache->initCache($cacheTtl, $uri, $path)) {
			$result = $cache->getVars();
		} elseif ($cache->startDataCache()) {
			$taggedCache = $aInstance->getTaggedCache();
			$taggedCache->startTagCache('kokoc.seo');
			$taggedCache->registerTag('kokoc.seo');
			$taggedCache->startTagCache($path);
			$taggedCache->registerTag('kokoc.seo.redirect');

			$ibmodule = null;
			$new_url = $elemetnID = $sectionID = $iblockID = false;
			$res = RT::getList(array('filter' => array('ACTIVE' => 'Y', 'URL' => $url, '!TYPE' => 'X')));
			if (!$rediect = $res->fetch()) { // del this?
				$res = RT::getList(array('filter' => array('ACTIVE' => 'Y', 'URL' => $uri, '!TYPE' => 'X')));
				$rediect = $res->fetch();
			}
			if ($rediect){
				$new_url = $rediect['NEW_URL'];
				switch ($rediect['TYPE']) {
					case 'E':
						if (is_null($ibmodule))
							$ibmodule = Loader::includeModule("iblock");
						if (!$ibmodule) break;
						if (isset($rediect['IB_ID']) &&
						($el = \Bitrix\Iblock\ElementTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('ID'=>$rediect['IB_ID'])
						))->fetch())){
							$elemetnID = $el['ID'];
							$iblockID = $el['IBLOCK_ID'];
						} elseif (isset($rediect['IB_XML_ID']) &&
						($el = \Bitrix\Iblock\ElementTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('XML_ID'=>$rediect['IB_XML_ID'])
						))->fetch())){
							$elemetnID = $el['ID'];
							$iblockID = $el['IBLOCK_ID'];
						} elseif (isset($rediect['IB_PAR_ID']) &&
						($sec = \Bitrix\Iblock\SectionTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('ID'=>$rediect['IB_PAR_ID'])
						))->fetch())){
							$sectionID = $sec['ID'];
							$iblockID = $el['IBLOCK_ID'];
						}
					break;
					case 'S':
						if (is_null($ibmodule))
							$ibmodule = Loader::includeModule("iblock");
						if (!$ibmodule) break;
						if (isset($rediect['IB_ID']) &&
						($sec = \Bitrix\Iblock\SectionTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('ID'=>$rediect['IB_ID'])
						))->fetch())){
							$sectionID = $sec['ID'];
							$iblockID = $el['IBLOCK_ID'];
						} elseif (isset($rediect['IB_XML_ID']) &&
						($sec = \Bitrix\Iblock\SectionTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('XML_ID'=>$rediect['IB_XML_ID'])
						))->fetch())){
							$sectionID = $sec['ID'];
							$iblockID = $el['IBLOCK_ID'];
						} elseif (isset($rediect['IB_PAR_ID']) && 
						($sec = \Bitrix\Iblock\SectionTable::getList(array(
							'select'=>array('ID', 'IBLOCK_ID'),
							'filter'=>array('ID'=>$rediect['IB_PAR_ID'])
						))->fetch())){
							$sectionID = $sec['ID'];
							$iblockID = $el['IBLOCK_ID'];
						}
					break;
					case 'X':
					case 'D':
					default:
					break;
				}
			}

			if ($elemetnID) {
				$res = CIBlockElement::GetList(false, array('ID' => $elemetnID), false, array('nTopCount'=>1), array('ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL'));
				if ($el = $res->GetNext()) {
					$new_url = $el['DETAIL_PAGE_URL'];
					$taggedCache->registerTag('iblock_id_'.$el['IBLOCK_ID']);
				}
			} elseif ($sectionID) {
				$res = CIBlockSection::GetList(false, array('ID' => $sectionID), false, array('nTopCount'=>1), array('ID', 'IBLOCK_ID', 'SECTION_PAGE_URL'));
				if ($sec = $res->GetNext()){
					$new_url = $sec['SECTION_PAGE_URL'];
					$taggedCache->registerTag('iblock_id_'.$sec['IBLOCK_ID']);
				}
			}

			if (!$new_url) {
				$res = RT::getList(array('filter' => array('ACTIVE' => 'Y', '=TYPE' => 'X')));
				while ($rediect = $res->fetch()) {
					$template = '#^'.str_replace('/', '\/', $rediect['URL']).'$#';
					if (preg_match($template, $url)){
						$new_url = preg_replace($template, $rediect['NEW_URL'], $url);
						break;
					}
				}
			}

			$result = array('URL' => $new_url, 'REDIRECT' => $rediect);
			$taggedCache->endTagCache();
			$taggedCache->endTagCache();
			$cache->endDataCache($result);
		}

		if ($result['URL'] && ($result['URL'] != $url)) {
			if ($result['REDIRECT']['ID']) {
				RT::update($result['REDIRECT']['ID'], array('COUNT' => new DB\SqlExpression('?# + 1', 'COUNT'), 'LAST_USE' => new DateTime));
			}

			$new_uri = ($request->isHttps()?'https':'http').'://'.$context->getServer()->getHttpHost().$result['URL'].(($string = $context->getServer()->get('QUERY_STRING'))?'?'.$string:'');
			if (!CHTTP::isPathTraversalUri($new_uri)) {
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: '.$new_uri);
				exit();
			}
		}

		return;
	}

	public static function setTrackIB()
	{
		$CEventM = EventManager::getInstance();
		$CEventM->AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("Kokoc\Seo\EventHandlers", "OnBeforeIBlockSectionUpdate"));
		$CEventM->AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", Array("Kokoc\Seo\EventHandlers", "OnAfterIBlockSectionUpdate"));
		$CEventM->AddEventHandler("iblock", "OnIBlockElementUpdate", Array("Kokoc\Seo\EventHandlers", "OnIBlockElementUpdate"));
		$CEventM->AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Kokoc\Seo\EventHandlers", "OnAfterIBlockElementUpdate"));
	}

	public static function checkSEF()
	{
		global $APPLICATION;

		$context = Context::getCurrent();
		$server = $context->getServer();
		$arServer = $server->toArray();

		if($context->getRequest()->isAjaxRequest()) 
			return;

		$url = parse_url($context->getRequest()->getRequestUri());
		$query = ST::getList(array(
			'select' => array('URL', 'NEW_URL'),
			'filter' => array('ACTIVE' => 'Y', 'URL' => $url['path']),
			'cache' => array('ttl' => Option::get(self::MODULE_ID, 'cache_ttl', 3600))
		));
		if ($result = $query->fetch()){
			if ($result['NEW_URL'] !== $result['URL']) {
				LocalRedirect($result['NEW_URL'].($url['query']?"?".$url['query']:''), false, '301 Moved Permanently' );
				die;
			}
		}
		$query = ST::getList(array(
			'select' => array('URL', 'NEW_URL', 'ADD_PARAMS', 'META_ID', 'M_' => 'META.*'),
			'filter' => array('ACTIVE' => 'Y',  'NEW_URL' => $url['path']),
			'cache' => array('ttl' => Option::get(self::MODULE_ID, 'cache_ttl', 3600))
		));
		if ($result = $query->fetch()){
			$_SERVER['REQUEST_URI'] = $result['URL'].($url['query']?"?".$url['query']:'');
			$arServer['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
			$server->set($arServer);
			if (!empty($result['ADD_PARAMS'])) {
				parse_str($result['ADD_PARAMS'], $addParams);
				$_GET = array_merge($_GET, $addParams);
				$_POST = array_merge($_POST, $addParams);
			}
			$context->initialize(new HttpRequest($server, $_GET, $_POST, $_FILES, $_COOKIE), $context->getResponse(), $server);
			$APPLICATION->reinitPath();

			if ($result['META_ID'])
				foreach ($result as $k => $v)
					if (mb_substr($k,0,2) == 'M_')
						Seo::setMeta(mb_substr($k,2),$v);
		}
	}

	public static function paginationSEF()
	{
		if (defined('ADMIN_SECTION')) return;
		global $APPLICATION;

		$context = Context::getCurrent();
		if($context->getRequest()->isAjaxRequest()) 
			return;
		$server = $context->getServer();
		$arServer = $server->toArray();
		$url = parse_url($context->getRequest()->getRequestUri());

		if (preg_match('#(.*)/page-([\d]+)/$#', $url['path'], $matches)){
			$new_url = $matches[1].'/';
			for ($i=1; $i<10; $i++)
				$GLOBALS['PAGEN_'.$i] = $matches[2];

			$_SERVER['REQUEST_URI'] = $new_url.($url['query']?"?".$url['query']:'');
			$arServer['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
			$server->set($arServer);

			$context->initialize(new HttpRequest($server, $_GET, $_POST, $_FILES, $_COOKIE), $context->getResponse(), $server);
			$APPLICATION->reinitPath();
		}
	}

	public static function JSCoreRegisterTranslit()
	{
		if (($core = getLocalPath('modules/'.self::MODULE_ID.'/js/core_translit.js'))
		&& (getLocalPath('modules/'.self::MODULE_ID.'/js/lang/'.LANGUAGE_ID.'/js_core_translit.php'))){
			CJSCore::RegisterExt('translit', array(
				'js' => $core,
				'lang' => getLocalPath('modules/'.self::MODULE_ID.'/js/js_core_translit.php'),
				'lang_additional' => array('YANDEX_KEY' => Option::get('main', 'translate_key_yandex')),
			));
		}
	}

	public static function checkMeta()
	{
		$url = Application::getInstance()->getContext()->getRequest()->getRequestedPageDirectory().'/';
		$res = MT::getList(array(
			'filter' => array('ACTIVE' => 'Y', 'URL' => $url),
			'cache' => array('ttl' => Option::get(self::MODULE_ID, 'cache_ttl', 3600))
		));
		if (($meta = $res->fetch()))
			Seo::setMeta($meta);
	}

	public static function setMataTags()
	{
		global $APPLICATION;
		if (($h1 = Seo::getMeta('h1')))
			$APPLICATION->SetTitle($h1);
		if (($title = Seo::getMeta('title')))
			$APPLICATION->SetPageProperty('title', $title);
		if (($description = Seo::getMeta('description')))
			$APPLICATION->SetPageProperty('description', $description);
		if (($keywords = Seo::getMeta('keywords')))
			$APPLICATION->SetPageProperty('keywords', $keywords);
		if (($canonical = Seo::getMeta('canonical')))
			$APPLICATION->SetPageProperty('canonical', $canonical);
	}

	public static function paginationAddMeta()
	{
		global $APPLICATION, $NavNum;
		for ($i = 1; $i <= $NavNum; $i++)
			if ($page = $GLOBALS['PAGEN_'.$i])
				break;

		if ($page > 1) {
			Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].dirname(getLocalPath('modules/'.self::MODULE_ID.'/include.php')).'/pagen_templates.php' );

			$replace = array(
				'#H1#' => $APPLICATION->GetTitle(false),
				'#TITLE#' => $APPLICATION->GetProperty('title'),
				'#DESCR#' => $APPLICATION->GetProperty('description'),
				'#KEYWR#' => $APPLICATION->GetProperty('keywords'),
				'#PAGE#' => $page
			);

			if (($template = Loc::GetMessage('PAGEN_TEMPLATE_H1', $replace)))
				$APPLICATION->SetTitle($template);
			if (($template = Loc::GetMessage('PAGEN_TEMPLATE_TITLE', $replace)))
				$APPLICATION->SetPageProperty('title', $template);
			if (($template = Loc::GetMessage('PAGEN_TEMPLATE_DESCR', $replace)))
				$APPLICATION->SetPageProperty('description', $template);
			if (($template = Loc::GetMessage('PAGEN_TEMPLATE_KEYWR', $replace)))
				$APPLICATION->SetPageProperty('keywords', $template);
		}
	}

	public static function forceChangeMeta(&$content)
	{
		global $APPLICATION;
		$h1 = $APPLICATION->GetTitle(false);
		$title = $APPLICATION->GetProperty('title');
		$descr = $APPLICATION->GetProperty('description');
		$keyw = $APPLICATION->GetProperty('keywords');
		$canon = $APPLICATION->GetProperty('canonical');

		if ((($pos = mb_strpos($content, '<h1')) !== false) &&
			(($pos = mb_strpos($content, '>',$pos)) !== false) &&
			(($pos2 = mb_strpos($content, '</h1>', $pos)) !== false)
		){
			$content = mb_substr($content,0,$pos+1).$h1.mb_substr($content,$pos2);
		}
		if ((($pos = mb_strpos($content, '<title>')) !== false) &&
			(($pos2 = mb_strpos($content, '</title>', $pos)) !== false)
		){
			$content = mb_substr($content,0,$pos+7).$title.mb_substr($content,$pos2);
		}
		if ($descr){
			if ((($pos = mb_strpos($content, '<meta name="description"')) !== false) &&
				(($pos = mb_strpos($content, 'content="', $pos)) !== false) &&
				(($pos2 = mb_strpos($content, '"', $pos+9)) !== false)
			){
				$content = mb_substr($content,0,$pos+9).$descr.mb_substr($content,$pos2);
			}
		} else {
			if ((($pos = mb_strpos($content, '<meta name="description"')) !== false) &&
				(($pos2 = mb_strpos($content, '>', $pos)) !== false)
			){
				$content = mb_substr($content,0,$pos).mb_substr($content,$pos2+1);
			}
		}
		if ($keyw){
			if ((($pos = mb_strpos($content, '<meta name="keywords"')) !== false) &&
				(($pos = mb_strpos($content, 'content="', $pos)) !== false) &&
				(($pos2 = mb_strpos($content, '"', $pos+9)) !== false)
			){
				$content = mb_substr($content,0,$pos+9).$keyw.mb_substr($content,$pos2);
			}
		} else {
			if ((($pos = mb_strpos($content, '<meta name="keywords"')) !== false) &&
				(($pos2 = mb_strpos($content, '>', $pos)) !== false)
			){
				$content = mb_substr($content,0,$pos).mb_substr($content,$pos2+1);
			}
		}
		if ($canon){
			if ((($pos = mb_strpos($content, '<link rel="canonical"')) !== false) &&
				(($pos = mb_strpos($content, 'href="', $pos)) !== false) &&
				(($pos2 = mb_strpos($content, '"', $pos+6)) !== false)
			){
				$content = mb_substr($content,0,$pos+6).$canon.mb_substr($content,$pos2);
			}
		}
	}

	public static function OnBeforeIBlockSectionUpdate($arFields)
	{
		if ($arFields['CODE'] || $arFields['IBLOCK_SECTION_ID'] || $arFields['XML_ID']){
			if (!$arFields['IBLOCK_ID'] || strpos(','.Option::get(self::MODULE_ID, 'redirect_track_ib', '').',', ','.$arFields['IBLOCK_ID'].',')!==false){
				self::$arIBlockSections[$arFields['ID']] = CIBlockSection::GetList(
					false, array('ID' => $arFields['ID']), false,
					array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL'), array('nTopCount' => 1)
				)->GetNext();
			}
		}
	}

	public static function OnAfterIBlockSectionUpdate($arFields)
	{
		if ($arFields['RESULT'] && self::$arIBlockSections[$arFields['ID']]){
			$section = CIBlockSection::GetList(
				false, array('ID' => $arFields['ID']), false,
				array('ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'SECTION_PAGE_URL'), array('nTopCount' => 1)
			)->GetNext();
			if ($section &&
				strpos(','.Option::get(self::MODULE_ID, 'redirect_track_ib', '').',', ','.$section['IBLOCK_ID'].',') !== false &&
				$section['SECTION_PAGE_URL'] != self::$arIBlockSections[$arFields['ID']]['SECTION_PAGE_URL']
			) {
				if (($redirect = RT::getList(array('filter' => array('URL' => self::$arIBlockSections[$arFields['ID']]['SECTION_PAGE_URL'], 'TYPE' => 'S')))->fetch())) {
					if (($redirect['IB_ID']!=$section['ID'])||($redirect['IB_XML_ID']!=$section['XML_ID'])||($redirect['IB_PAR_ID']!=$section['IBLOCK_SECTION_ID']))
						RT::update($redirect['ID'], array(
							'IB_ID' => $section['ID'].'',
							'IB_XML_ID' => $section['XML_ID'].'',
							'IB_PAR_ID' => $section['IBLOCK_SECTION_ID'].'',
							'COUNT' => 0
						));
				} else {
					RT::add(array(
						'URL' => self::$arIBlockSections[$arFields['ID']]['SECTION_PAGE_URL'],
						'TYPE' => 'S',
						'IB_ID' => $section['ID'].'',
						'IB_XML_ID' => $section['XML_ID'].'',
						'IB_PAR_ID' => $section['IBLOCK_SECTION_ID'].'',
					));
				}
				if (!isset(self::$arIBlocks[$arFields['IBLOCK_ID']])) {
					self::$arIBlocks[$arFields['IBLOCK_ID']] = CIBlock::GetByID($arFields['IBLOCK_ID'])->GetNext();
				}
				if (strpos(self::$arIBlocks[$arFields['IBLOCK_ID']]['SECTION_PAGE_URL'], 'SECTION_CODE_PATH') !== false
					|| strpos(self::$arIBlocks[$arFields['IBLOCK_ID']]['DETAIL_PAGE_URL'], 'SECTION_CODE_PATH') !== false
				) {
					$template = self::$arIBlockSections[$arFields['ID']]['SECTION_PAGE_URL'] . '(.*)/';
					$replace = $section['SECTION_PAGE_URL'] . '${1}/';
					if (($redirect = RT::getList(array('filter' => array('URL' => $template, 'TYPE' => 'X')))->fetch())) {
						if ($redirect['NEW_URL'] != $replace)
							RT::update($redirect['ID'], array(
								'NEW_URL' => $replace,
								'COUNT' => 0
							));
					} else {
						RT::add(array(
							'URL' => $template,
							'NEW_URL' => $replace,
							'TYPE' => 'X'
						));
					}
				}
			}
		}
	}

	public static function OnIBlockElementUpdate($arFields, $ar_wf_element)
	{
		if ($arFields['CODE'] || $arFields['SECTION_ID'] || $arFields['XML_ID']){
			if (!$arFields['IBLOCK_ID'] || strpos(','.Option::get(self::MODULE_ID, 'redirect_track_ib', '').',', ','.$arFields['IBLOCK_ID'].',')!==false){
				self::$arIBlockElements[$arFields['ID']] = CIBlockElement::GetList(
					false, array('ID' => $arFields['ID']), false, array('nTopCount' => 1),
					array('ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL')
				)->GetNext();
			}
		}
	}

	public static function OnAfterIBlockElementUpdate($arFields)
	{
		if ($arFields['RESULT'] && self::$arIBlockElements[$arFields['ID']]){
			$element = CIBlockElement::GetList(
				false, array('ID' => $arFields['ID']), false, array('nTopCount' => 1),
				array('ID', 'IBLOCK_ID', 'XML_ID', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL')
			)->GetNext();
			if ($element &&
				strpos(','.Option::get(self::MODULE_ID, 'redirect_track_ib', '').',', ','.$element['IBLOCK_ID'].',') !== false &&
				$element['DETAIL_PAGE_URL'] != self::$arIBlockElements[$arFields['ID']]['DETAIL_PAGE_URL']
			) {
				if (($redirect = RT::getList(array('filter' => array('URL' => self::$arIBlockElements[$arFields['ID']]['DETAIL_PAGE_URL'], 'TYPE' => 'E')))->fetch())) {
					if (($redirect['IB_ID']!=$element['ID'])||($redirect['IB_XML_ID']!=$element['XML_ID'])||($redirect['IB_PAR_ID']!=$element['IBLOCK_SECTION_ID']))
						RT::update($redirect['ID'], array(
							'IB_ID' => $element['ID'].'',
							'IB_XML_ID' => $element['XML_ID'].'',
							'IB_PAR_ID' => $element['IBLOCK_SECTION_ID'].'',
							'COUNT' => 0
						));
				} else {
					RT::add(array(
						'URL' => self::$arIBlockElements[$arFields['ID']]['DETAIL_PAGE_URL'],
						'TYPE' => 'E',
						'IB_ID' => $element['ID'].'',
						'IB_XML_ID' => $element['XML_ID'].'',
						'IB_PAR_ID' => $element['IBLOCK_SECTION_ID'].'',
					));
				}
			}
		}
	}
}
