<?php
namespace Kokoc;

use CUser,
	COption,
	Bitrix\Main,
	Bitrix\Main\Entity,
	Bitrix\Main\Application,
	Bitrix\Main\Config\Option,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\SefTable,
	Kokoc\Seo\MetaTable,
	Kokoc\Seo\RedirectTable,
	Kokoc\Seo\DeclensionTable,
	phpMorphy;

Loc::loadMessages(__FILE__);

class Seo
{
	const MODULE_ID = 'kokoc.seo';

	private static $arMeta = array();
	private static $CMorfer = null;

	public static function ClearModuleCache()
	{
		$tagged = Application::getInstance()->getTaggedCache();
		$tagged->clearByTag('kokoc.seo');

		SefTable::getEntity()->cleanCache();
		MetaTable::getEntity()->cleanCache();
		RedirectTable::getEntity()->cleanCache();
		DeclensionTable::getEntity()->cleanCache();
	}

	public static function setMeta($meta, $value = '')
	{
		if (empty($meta)) return;
		if (!is_array($meta))
			$meta = array($meta => $value);

		foreach($meta as $tag => $value)
			self::$arMeta[strtoupper($tag)] = $value;
	}

	public static function getMeta($key)
	{
		$key = strtoupper($key);
		if (array_key_exists($key, self::$arMeta))
			return self::$arMeta[$key];
		else
			return false;
	}

	public static function getSeoText($position)
	{
		$position = strtoupper('TEXT_'.$position);
		if (array_key_exists($position, self::$arMeta)){
			if (empty(($text = self::$arMeta[$position])))
				return;
			if (strtoupper(self::$arMeta[$position.'_TYPE']) == 'HTML')
				$text = html_entity_decode($text);
			return $text;
		}
		return;
	}

	public static function getCase($value, $case, $gender = null, $number = null, $type = null, $generate = null, $save = null, &$manual = false)
	{
		if (!$value) return '';
		if (!$case) return $value;
		if (is_null($generate))
			$generate = Option::get(self::MODULE_ID, 'generate_declensions', true);
		if (is_null($save))
			$save = Option::get(self::MODULE_ID, 'save_declensions', false);
		$generate = $generate === true;
		$save = $save === true;

		$aInstance = Application::getInstance();
		$cache = $aInstance->getCache();
		$cacheTtl = Option::get(self::MODULE_ID, 'cache_ttl', 3600);
		$path = 'kokoc.seo/declensions';
		if ($cache->initCache($cacheTtl, implode('',func_get_args()), $path)) {
			extract($cache->getVars());
		} elseif ($cache->startDataCache()) {
			$taggedCache = $aInstance->getTaggedCache();
			$taggedCache->startTagCache('kokoc.seo');
			$taggedCache->registerTag('kokoc.seo');
			$taggedCache->startTagCache($path);
			$taggedCache->registerTag('kokoc.seo.declension');
			$taggedCache->endTagCache();
			$taggedCache->endTagCache();

			$arCases = array('именительный'=>'ИМ','им'=>'ИМ','родительный'=>'РД','рд'=>'РД','дательный'=>'ДТ','дт'=>'ДТ','винительный'=>'ВН','вн'=>'ВН','творительный'=>'ТВ','тв'=>'ТВ','предложный'=>'ПР','пр'=>'ПР');
			if (in_array($case, array_keys($arCases)))
				$case = $arCases[$case];
			$filter = array(
				'TARGET' => $value,
				'CASE' => $case
			);
			$arGenders = array('A'=>'A','m'=>'M','M'=>'M','МР'=>'M','муж'=>'M','w'=>'W','W'=>'W','ЖР'=>'W','жен'=>'W','n'=>'N','N'=>'N','СР'=>'N','ср'=>'N');
			if (!is_null($gender) && (in_array($gender, array_keys($arGenders)))){
				$filter['GENDER'] = $arGenders[$gender];
			} 
			$arNumbers = array('s'=>'S','S'=>'S','ед'=>'S','p'=>'P','P'=>'P','мн'=>'P');
			if (!is_null($number) && (in_array($number, array_keys($arNumbers)))){
				$filter['NUMBER'] = $arNumbers[$number];
			}
			if (!is_null($type)){
				$filter['TYPE'] = $type;
			}

			$dbres = DeclensionTable::getList(array('filter' => $filter, 'limit' => 1));
			if (($dtresult = $dbres->fetch())){
				$return = $dtresult['VALUE'];
				if ($dtresult['GENERATE'] == 'M')
					$manual = true;
				else
					$manual = false;
			}

			if (!$dtresult && $generate){
				$c = $n = $g = false;
				if (in_array($case, $arCases))
					$c = $case;
				if (isset($filter['NUMBER']))
					$n = ($filter['NUMBER']=='P' ? 'МН' : 'ЕД');
				if (isset($filter['GENDER']) && in_array($filter['GENDER'], array('M', 'W', 'N')))
					$g = ($filter['GENDER']=='W'?'ЖР':($filter['GENDER']=='N'?'СР':'МР'));

				$return = self::getWordsCase($value, $c, $n, $g);

				if ($save && ($value !== $return))
					DeclensionTable::add(array_merge($filter, array('VALUE' => $return)));
			}
			
			$result = array('return' => $return, 'manual' => $manual);
			$cache->endDataCache($result);
		}

		return $return;
	}

	public static function getWordsCase($value, $case = false, $number = false, $gender = false) {
		if (!$value) return '';
		if (!($case || $number || $gender))
			return self::getBaseForm($value);

		$morphy = self::getMorpher();
		if (!($morphy instanceof phpMorphy)){
			return $value;
		}

		if ($number && !in_array($number, array('МН','ЕД')))
			$number = false;
		if (!$case || !in_array($case, array('ИМ','РД','ДТ','ВН','ТВ','ПР')))
			$case = 'ИМ';

		$returnString = $value;
		$words = $arName = preg_split("/[\s,]+/u", $value);
		foreach ($words as $k => $word)
			if (preg_match('/[a-zA-Z0-9]+/i',$word)){
				unset ($words[$k]);
				unset ($arName[$k]);
			}
			else 
				$words[$k] = mb_strtoupper($word);
		foreach ($arName as $k => $word)
			$returnString = str_replace($word, '#rep'.$k.'#', $returnString);

		if ($gender && in_array($gender, array('МР', 'ЖР', 'СР'))) {
			$genre = $gender;
		} else {
			$genre = 'МР';
			$paradigms_frase = $morphy->findWord($words);
			foreach ($paradigms_frase as $paradigms) {
				if (!$paradigms) continue;
				if (is_object($paradigm = $paradigms->getByPartOfSpeech('С')[0])) {
					if ($paradigm->hasGrammems('ЖР'))
						$genre = 'ЖР';
					elseif ($paradigm->hasGrammems('СР'))
						$genre = 'СР';
					else
						$genre = 'МР';
					break;
				}
			}
		}

		$grammems = array($case);
		if ($number) $grammems[] = $number;
		foreach ($words as $k => $word) {
			$arCandidates = $morphy->castFormByGramInfo($word, null, $grammems);
			foreach($arCandidates as $candidate) {
				if ($candidate['pos'] == 'П') {
					if (in_array($genre, $candidate['grammems'])){
						$arWords[$k] = $candidate['form'];
						continue(2);
					}
				} else {
					$arWords[$k] = $candidate['form'];
					continue(2);
				}
			}
			foreach($arCandidates as $candidate) { // если не найдено прилагательное определенного рода
				if ($candidate['pos'] == 'П') {
					if (in_array('НО', $candidate['grammems'])){
						$arWords[$k] = $candidate['form'];
						continue(2);
					}
				}
			}
		}

		foreach ($arWords as $k => $word) {
			if ($arName[$k] != mb_strtoupper($arName[$k]))
				$word = mb_strtolower($word);
			if (($letter = mb_substr($arName[$k],0,1)) == mb_strtoupper($letter))
				$word = mb_strtoupper(mb_substr($word,0,1)).mb_substr($word,1);

			$returnString = str_replace('#rep'.$k.'#', $word, $returnString);
		}

		return($returnString);
	}

	public static function getBaseForm($value) {
		if (!$value) return '';
		$morphy = self::getMorpher();
		if (!($morphy instanceof phpMorphy)){
			return $value;
		}

		$returnString = $value;
		$words = $arName = preg_split("/[\s,]+/u", $value);
		foreach ($words as $k => $word)
			if (preg_match('/[a-zA-Z0-9]+/i',$word)){
				unset ($words[$k]);
				unset ($arName[$k]);
			}
			else 
				$words[$k] = mb_strtoupper($word);
		foreach ($arName as $k => $word)
			$returnString = str_replace($word, '#rep'.$k.'#', $returnString);

		$lemmatized = $morphy->lemmatize($words);

		foreach ($words as $k => $word) {
			if (empty($lemmatized[$word][0])) {
				$word = $arName[$k];
			} else {
				$word = $lemmatized[$word][0];
				if ($arName[$k] != mb_strtoupper($arName[$k]))
					$word = mb_strtolower($word);
				if (($letter = mb_substr($arName[$k],0,1)) == mb_strtoupper($letter))
					$word = mb_strtoupper(mb_substr($word,0,1)).mb_substr($word,1);
			}

			$returnString = str_replace('#rep'.$k.'#', $word, $returnString);
		}
		return $returnString;
	}

	private static function getMorpher() {
		if (isset(self::$CMorfer))
			return self::$CMorfer;
		self::$CMorfer = false;
		if (($common = getLocalPath('modules/'.self::MODULE_ID.'/phpmorphy/src/common.php')))
			require_once($_SERVER["DOCUMENT_ROOT"].$common);
		else
			return false;
		$dir = mb_substr($_SERVER["DOCUMENT_ROOT"].$common, 0, -14);

		$opts = array(
			'storage' => PHPMORPHY_STORAGE_FILE,
			'with_gramtab' => false,
			'predict_by_suffix' => true, 
			'predict_by_db' => true
		);
		try {
			self::$CMorfer = new phpMorphy($dir.'dicts', 'ru_RU', $opts);
		} catch(phpMorphy_Exception $e) {
			self::$CMorfer = $e->getMessage();
		}
		return self::$CMorfer;
	}

	public static function translit($str)
	{
		$trans_from = explode(',','а,б,в,г,д,е,ё,ж,з,и,й,к,л,м,н,о,п,р,с,т,у,ф,х,ц,ч,ш,щ,ъ,ы,ь,э,ю,я,А,Б,В,Г,Д,Е,Ё,Ж,З,И,Й,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ч,Ш,Щ,Ъ,Ы,Ь,Э,Ю,Я,і,І,ї,Ї,ґ,Ґ');
		$trans_to = explode(',', 'a,b,v,g,d,e,yo,zh,z,i,j,k,l,m,n,o,p,r,s,t,u,f,h,c,ch,sh,shch,,y,,eh,yu,ya,A,B,V,G,D,E,YO,ZH,Z,I,J,K,L,M,N,O,P,R,S,T,U,F,H,C,CH,SH,SHCH,,Y,,EH,YU,YA,i,I,i,I,g,G');
		foreach($trans_from as $i => $from)
			$search[$from] = $trans_to[$i];

		$params = array(
			"max_len" => 100,
			"change_case" => 'L', // 'L' - toLower, 'U' - toUpper, false - do not change
			"replace_space" => '-',
			"replace_other" => '-',
			"delete_repeat_replace" => true,
			"safe_chars" => '',
		);

		$len = strlen($str);
		$str_new = '';
		$last_chr_new = '';

		for($i = 0; $i < $len; $i++)
		{
			$chr = substr($str, $i, 1);

			if(preg_match("/[a-zA-Z0-9]/".BX_UTF_PCRE_MODIFIER, $chr) || strpos($params["safe_chars"], $chr)!==false)
			{
				$chr_new = $chr;
			}
			elseif(preg_match("/\\s/".BX_UTF_PCRE_MODIFIER, $chr))
			{
				if (
					!$params["delete_repeat_replace"]
					||
					($i > 0 && $last_chr_new != $params["replace_space"])
				)
					$chr_new = $params["replace_space"];
				else
					$chr_new = '';
			}
			else
			{
				if(array_key_exists($chr, $search))
				{
					$chr_new = $search[$chr];
				}
				else
				{
					if (
						!$params["delete_repeat_replace"]
						||
						($i > 0 && $i != $len-1 && $last_chr_new != $params["replace_other"])
					)
						$chr_new = $params["replace_other"];
					else
						$chr_new = '';
				}
			}

			if ((($chr_new == 'h')||($chr_new == 'H')) && (in_array(substr($last_chr_new, -1), array('c','s','e','h','C','S','E','H')))){
				if ($chr_new == 'H'){
					$chr_new = 'KH';
				} else {
					$chr_new = 'kh';
				}
			}

			if(strlen($chr_new))
			{
				if($params["change_case"] == "L" || $params["change_case"] == "l")
					$chr_new = ToLower($chr_new);
				elseif($params["change_case"] == "U" || $params["change_case"] == "u")
					$chr_new = ToUpper($chr_new);

				$str_new .= $chr_new;
				$last_chr_new = $chr_new;
			}

			if (strlen($str_new) >= $params["max_len"])
				break;
		}

		return $str_new;
	}
}
