<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Text\String;

defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'kokoc.seo');

if (!$USER->isAdmin()) {
	$APPLICATION->authForm('Nope');
}

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

Loc::loadMessages($context->getServer()->getDocumentRoot().'/bitrix/modules/main/options.php');
Loc::loadMessages(__FILE__);

$tabControl = new CAdminTabControl('tabControl', array(
	array(
		'DIV' => 'edit1',
		'TAB' => Loc::getMessage('MAIN_TAB_SET'),
		'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_SET'),
	),
	array(
		'DIV' => 'cache',
		'TAB' => Loc::getMessage('KS_CACHE_TAB'),
		'TITLE' => Loc::getMessage('KS_CACHE_TAB'),
	),
));

$arAllOptions = array(
	array('module_active', GetMessage('KS_MODULE_ACTIVE'), 'Y', array('checkbox', 'Y')),
	GetMessage('KS_REDIRECTS'),
	array('use_redirects', GetMessage('KS_REDIRECTS_ACTIVE'), 'N', array('checkbox', 'Y')),
	array('redirect_track_ib', GetMessage('KS_REDIRECTS_TRACK_ACTIVE'), '', array('text', '50')),
	GetMessage('KS_META'),
	array('meta_active', GetMessage('KS_META_ACTIVE'), 'N', array('checkbox', 'Y')),
	array('meta_force_rewrite', GetMessage('KS_META_FORCE'), 'N', array('checkbox', 'Y')),
	GetMessage('KS_SEF'),
	array('use_sef_spoof', GetMessage('KS_SEF_ACTIVE'), 'N', array('checkbox', 'Y')),
	GetMessage('KS_OTHER'),
	array('change_admin_translit', GetMessage('KS_TRANSLIT_ACTIVE'), 'N', array('checkbox', 'Y')),
	array('seo_pagination_active', GetMessage('KS_PAGINATION_ACTIVE'), 'N', array('checkbox', 'Y')),
);
$arCacheOptions = array(
	array('cache_ttl', GetMessage('KS_CACHE_TTL'), '3600', array('text', '10')),
);

if($_SERVER['REQUEST_METHOD']=='POST' && strlen($_POST['Update'])>0 && $USER->CanDoOperation('edit_other_settings') && check_bitrix_sessid())
{
	foreach($arAllOptions as $option)
	{
		__AdmSettingsSaveOption(ADMIN_MODULE_NAME, $option);
	}
	foreach($arCacheOptions as $option)
	{
		__AdmSettingsSaveOption(ADMIN_MODULE_NAME, $option);
	}
} elseif ($_SERVER['REQUEST_METHOD']=='POST' && strlen($_POST['clearCache'])>0 && $USER->CanDoOperation('edit_other_settings') && check_bitrix_sessid())
{
	Kokoc\Seo::ClearModuleCache();
}
if (Option::get(ADMIN_MODULE_NAME, 'seo_pagination_active') == 'Y'){
	$arAllOptions[] = array('note' => GetMessage('KS_PAGINATION_TEMPLATES').dirname(getLocalPath('modules/'.ADMIN_MODULE_NAME.'/include.php')).'/lang/'.LANGUAGE_ID.'/pagen_templates.php');
}

$tabControl->begin();
?>

<form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), urlencode($mid), LANGUAGE_ID)?>">
	<?php
	echo bitrix_sessid_post();
	$tabControl->beginNextTab();
	?>
	<?
	__AdmSettingsDrawList(ADMIN_MODULE_NAME, $arAllOptions);
	?>
	<?php
	$tabControl->beginNextTab();
	?>
	<?
	__AdmSettingsDrawList(ADMIN_MODULE_NAME, $arCacheOptions);
	?>
	<input type="submit"
			name="clearCache"
			value="<?=Loc::getMessage('KS_CACHE_BUT') ?>"
			title="<?=Loc::getMessage('KS_CACHE_BUT') ?>"
			/>
	<?php
	$tabControl->buttons();
	?>
	<input type="submit"
			name="Update"
			value="<?=Loc::getMessage('MAIN_SAVE') ?>"
			title="<?=Loc::getMessage('MAIN_OPT_SAVE_TITLE') ?>"
			class="adm-btn-save"
			/>
	<?php
	$tabControl->end();
	?>
</form>
