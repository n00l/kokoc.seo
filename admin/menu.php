<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$menu = array(
	array(
		'parent_menu' => 'global_menu_marketing',
		'sort' => 1100,
		'text' => Loc::getMessage('KOKOC_SEO_MENU_TITLE'),
		'title' => Loc::getMessage('KOKOC_SEO_MENU_TITLE'),
		'icon' => 'sale_menu_icon_bigdata',
		'page_icon' => 'default_page_icon',
		'items_id' => 'menu_scores',
		'items' => array(
			array(
				'text' => Loc::getMessage('KOKOC_SEO_SUBMENU_REDIRECTS_TEXT'),
				'url' => 'kokoc_seo_redirects.php',
				'title' => Loc::getMessage('KOKOC_SEO_SUBMENU_REDIRECTS_TITLE'),
			),
			array(
				'text' => Loc::getMessage('KOKOC_SEO_SUBMENU_SEF_TEXT'),
				'url' => 'kokoc_seo_sefs.php',
				'title' => Loc::getMessage('KOKOC_SEO_SUBMENU_SEF_TITLE'),
			),
			array(
				'text' => Loc::getMessage('KOKOC_SEO_SUBMENU_META_TEXT'),
				'url' => 'kokoc_seo_metas.php',
				'title' => Loc::getMessage('KOKOC_SEO_SUBMENU_META_TITLE'),
			),
			array(
				'text' => Loc::getMessage('KOKOC_SEO_SUBMENU_DECLENSIONS_TEXT'),
				'url' => 'kokoc_seo_declensions.php',
				'title' => Loc::getMessage('KOKOC_SEO_SUBMENU_DECLENSIONS_TITLE'),
			),
		),
	),
);

return $menu;