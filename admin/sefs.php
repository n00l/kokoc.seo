<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\SefTable;

$ModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($ModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

if (COption::GetOptionString('kokoc.seo', 'use_sef_spoof', false) !== 'Y')
	$strWarning = Loc::getMessage("KOKOC_SEO_REDIRECTS_OFF");

$sTableID = "kk_seo_sefs_list";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_universal",
	"filter_id_from",
	"filter_id_to",
	"filter_active",
	"filter_url",
	"filter_new_url",
	"filter_meta_id",
);

$lAdmin->InitFilter($arFilterFields);

$getListParams = array();
if(IntVal($filter_id_from)>0) $getListParams['filter'][">=ID"] = IntVal($filter_id_from);
if(IntVal($filter_id_to)>0) $getListParams['filter']["<=ID"] = IntVal($filter_id_to);
if(strlen($filter_active)>0) $getListParams['filter']["=ACTIVE"] = ($filter_active == 'Y' ? 'Y' : 'N');
if(strlen($filter_url)>0) $getListParams['filter']["?URL"] = $filter_url;
if(strlen($filter_new_url)>0) $getListParams['filter']["?NEW_URL"] = $filter_new_url;
if(IntVal($filter_meta_id)>0) $getListParams['filter']["=META_ID"] = IntVal($filter_meta_id);

if($lAdmin->EditAction() && $ModulePermissions=="W")
{
	// TODO: add edit
}

if(($arID = $lAdmin->GroupAction()) && $ModulePermissions=="W")
{
	if ($_REQUEST['action'] == 'delete') {
		foreach ($arID as $id) {
			SefTable::delete($id);
		}
	} elseif ($_REQUEST['action'] == 'activate') {
		foreach ($arID as $id) {
			if (($row = SefTable::getRowById($id)) && ($row['ACTIVE'] == 'N'))
			{
				SefTable::update($id, array('ACTIVE' => 'Y'));
			}
		}
	} elseif ($_REQUEST['action'] == 'deactivate') {
		foreach ($arID as $id) {
			if (($row = SefTable::getRowById($id)) && ($row['ACTIVE'] == 'Y'))
			{
				SefTable::update($id, array('ACTIVE' => 'N'));
			}
		}
	}
}

$lAdmin->AddHeaders(array(
	array("id"=>"ID",
		"content"=>"ID",
		"sort"=>"ID",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"ACTIVE",
		"content"=>Loc::getMessage("HEAD_ACTIVE"),
		"sort"=>"ACTIVE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"URL",
		"content"=>Loc::getMessage("HEAD_URL"),
		"sort"=>"URL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"NEW_URL",
		"content"=>Loc::getMessage("HEAD_NEW_URL"),
		"sort"=>"NEW_URL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"ADD_PARAMS",
		"content"=>Loc::getMessage("HEAD_ADD_PARAMS"),
		"sort"=>"ADD_PARAMS",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"META_ID",
		"content"=>Loc::getMessage("HEAD_META_ID"),
		"sort"=>"IB_META_ID",
		"align"=>"right",
		"default"=>true,
	),
));

$getListParams['select'] = ['*'];
$getListParams['order'] = [$lAdmin->sort->getField()=>$lAdmin->sort->getOrder()];

$dbSeoList = new CAdminResult(SefTable::getList($getListParams), $sTableID);

if ($usePageNavigation)
{
	$dbSeoList->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
	$dbSeoList->NavRecordCount = $totalCount;
	$dbSeoList->NavPageCount = $totalPages;
	$dbSeoList->NavPageNomer = $navyParams['PAGEN'];
	$dbSeoList->nSelectedCount = $totalCount;
}
else
{
	$dbSeoList->NavStart();
}
$lAdmin->NavText($dbSeoList->GetNavPrint(Loc::getMessage("SALE_PRLIST")));

while ($arSeo = $dbSeoList->NavNext())
{
	$row =& $lAdmin->AddRow($arSeo['ID'], $arSeo);

	$row->AddField("ID", '<a href="/bitrix/admin/kokoc_seo_sef.php?ID='.$arSeo['ID'].'">'.$arSeo['ID'].'</a>');
	$row->AddField("ACTIVE", $arSeo['ACTIVE']);
	$row->AddField("URL", $arSeo['URL']);
	$row->AddField("NEW_URL", $arSeo['NEW_URL']);
	$row->AddField("ADD_PARAMS", $arSeo['ADD_PARAMS']);
	$row->AddField("IB_META_ID", $arSeo['IB_META_ID']);
}

$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$dbSeoList->SelectedRowsCount()), 
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"),
	"activate"=>GetMessage("KOKOC_LIST_ACTIVATE"),
	"deactivate"=>GetMessage("KOKOC_LIST_DEACTIVATE"),
));

$aButtons = array(
	array(
		"ICON" => "btn_new",
		"TEXT" => GetMessage("KOKOC_ADDEL_TEXT"),
		"LINK" => "/bitrix/admin/kokoc_seo_sef.php",
		"LINK_PARAM" => "",
		"TITLE" => GetMessage("KOKOC_ADDEL_TITLE")
	),
	array("SEPARATOR" => true),
	array(
		"ICON"=>"btn_copy",
		"TEXT"=>GetMessage("KOKOC_IMPORT_TEXT"),
		"TITLE"=>GetMessage("KOKOC_IMPORT_TITLE"),
		"ONCLICK"=>"jsUtils.OpenWindow('".CUtil::JSEscape(getLocalPath('modules/kokoc.seo/admin/import_csv.php'))."?table=sef', 1200, 500)",
	),
);
$lAdmin->AddAdminContextMenu($aButtons);

$lAdmin->CheckListMode();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage("FILTER_H_ID"),
		GetMessage("FILTER_H_URL"),
		GetMessage("FILTER_H_NEW_URL"),
		GetMessage("FILTER_H_META_ID"),
		GetMessage("FILTER_H_ACTIVE"),
	)
);?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?$oFilter->Begin();?>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_id_from_Change()
				{
					if(document.find_form.filter_id_to.value.length<=0)
					{
						document.find_form.filter_id_to.value = document.find_form.filter_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("FILTER_ID_FROM");?>
			<input type="text" name="filter_id_from" OnChange="filter_id_from_Change()" value="<?echo (IntVal($filter_id_from)>0)?IntVal($filter_id_from):""?>" size="10">
			<?echo Loc::getMessage("FILTER_ID_TO");?>
			<input type="text" name="filter_id_to" value="<?echo (IntVal($filter_id_to)>0)?IntVal($filter_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_URL");?>:</td>
		<td>
			<input type="text" name="filter_url" value="<?echo (strlen($filter_url)>0)?$filter_url:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_NEW_URL");?>:</td>
		<td>
			<input type="text" name="filter_new_url" value="<?echo (strlen($filter_new_url)>0)?$filter_new_url:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_META_ID");?>:</td>
		<td>
			<input type="text" name="filter_meta_id" value="<?echo (strlen($filter_meta_id)>0)?$filter_meta_id:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ACTIVE")?>:</td>
		<td>
			<select name="filter_active">
				<option value=""><?echo Loc::getMessage("FILTER_ACTIVE_ALL")?></option>
				<option value="Y"<?if($filter_active=="Y") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_YES")?></option>
				<option value="N"<?if($filter_active=="N") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_NO")?></option>
			</select>
		</td>
	</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?CAdminMessage::ShowOldStyleError($error);?>
<?$lAdmin->DisplayList();?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
