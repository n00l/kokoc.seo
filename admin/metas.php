<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\MetaTable;

$ModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($ModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

if (COption::GetOptionString('kokoc.seo', 'meta_active', false) !== 'Y')
	$strWarning = Loc::getMessage("KOKOC_SEO_META_OFF");

$sTableID = "kk_seo_metas_list";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_id_from",
	"filter_id_to",
	"filter_active",
	"filter_url",
	"filter_canonical",
);

$lAdmin->InitFilter($arFilterFields);

$getListParams = array();
if(IntVal($filter_id_from)>0) $getListParams['filter'][">=ID"] = IntVal($filter_id_from);
if(IntVal($filter_id_to)>0) $getListParams['filter']["<=ID"] = IntVal($filter_id_to);
if(strlen($filter_active)>0) $getListParams['filter']["=ACTIVE"] = ($filter_active == 'Y' ? 'Y' : 'N');
if(strlen($filter_url)>0) $getListParams['filter']["?URL"] = $filter_url;
if(IntVal($filter_canonical)>0) $getListParams['filter']["=CANONICAL"] = IntVal($filter_canonical);

if($lAdmin->EditAction() && $ModulePermissions=="W")
{
	// TODO: add edit
}

if(($arID = $lAdmin->GroupAction()) && $ModulePermissions=="W")
{
	if ($_REQUEST['action'] == 'delete') {
		foreach ($arID as $id) {
			MetaTable::delete($id);
		}
	} elseif ($_REQUEST['action'] == 'activate') {
		foreach ($arID as $id) {
			if (($row = MetaTable::getRowById($id)) && ($row['ACTIVE'] == 'N'))
			{
				MetaTable::update($id, array('ACTIVE' => 'Y'));
			}
		}
	} elseif ($_REQUEST['action'] == 'deactivate') {
		foreach ($arID as $id) {
			if (($row = MetaTable::getRowById($id)) && ($row['ACTIVE'] == 'Y'))
			{
				MetaTable::update($id, array('ACTIVE' => 'N'));
			}
		}
	}
}

$lAdmin->AddHeaders(array(
	array("id"=>"ID",
		"content"=>"ID",
		"sort"=>"ID",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"ACTIVE",
		"content"=>Loc::getMessage("HEAD_ACTIVE"),
		"sort"=>"ACTIVE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"URL",
		"content"=>Loc::getMessage("HEAD_URL"),
		"sort"=>"URL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"H1",
		"content"=>Loc::getMessage("HEAD_H1"),
		"sort"=>"H1",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"TITLE",
		"content"=>Loc::getMessage("HEAD_TITLE"),
		"sort"=>"TITLE",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"DESCRIPTION",
		"content"=>Loc::getMessage("HEAD_DESCRIPTION"),
		"sort"=>"DESCRIPTION",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"KEYWORDS",
		"content"=>Loc::getMessage("HEAD_KEYWORDS"),
		"sort"=>"KEYWORDS",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"CANONICAL",
		"content"=>Loc::getMessage("HEAD_CANONICAL"),
		"sort"=>"CANONICAL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"TEXT_TOP",
		"content"=>Loc::getMessage("HEAD_TEXT_TOP"),
		"sort"=>"TEXT_TOP",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"TEXT_MIDDLE",
		"content"=>Loc::getMessage("HEAD_TEXT_MIDDLE"),
		"sort"=>"TEXT_MIDDLE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"TEXT_BOTTOM",
		"content"=>Loc::getMessage("HEAD_TEXT_BOTTOM"),
		"sort"=>"TEXT_BOTTOM",
		"align"=>"center",
		"default"=>true,
	),
));

$getListParams['select'] = ['*'];
$getListParams['order'] = [$lAdmin->sort->getField()=>$lAdmin->sort->getOrder()];

$dbSeoList = new CAdminResult(MetaTable::getList($getListParams), $sTableID);

if ($usePageNavigation)
{
	$dbSeoList->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
	$dbSeoList->NavRecordCount = $totalCount;
	$dbSeoList->NavPageCount = $totalPages;
	$dbSeoList->NavPageNomer = $navyParams['PAGEN'];
	$dbSeoList->nSelectedCount = $totalCount;
}
else
{
	$dbSeoList->NavStart();
}
$lAdmin->NavText($dbSeoList->GetNavPrint(Loc::getMessage("SALE_PRLIST")));

while ($arSeo = $dbSeoList->NavNext())
{
	$row =& $lAdmin->AddRow($arSeo['ID'], $arSeo);

	$row->AddField("ID", '<a href="/bitrix/admin/kokoc_seo_meta.php?ID='.$arSeo['ID'].'">'.$arSeo['ID'].'</a>');
	$row->AddField("ACTIVE", $arSeo['ACTIVE']);
	$row->AddField("URL", $arSeo['URL']);
	$row->AddField("H1", $arSeo['H1']);
	$row->AddField("TITLE", $arSeo['TITLE']);
	$row->AddField("DESCRIPTION", $arSeo['DESCRIPTION']);
	$row->AddField("KEYWORDS", $arSeo['KEYWORDS']);
	$row->AddField("CANONICAL", $arSeo['CANONICAL']);
	$row->AddField("TEXT_TOP", (empty($arSeo['TEXT_TOP'])?Loc::getMessage("TEXT_EMPTY"):Loc::getMessage("TEXT_N_EMPTY")));
	$row->AddField("TEXT_MIDDLE", (empty($arSeo['TEXT_MIDDLE'])?Loc::getMessage("TEXT_EMPTY"):Loc::getMessage("TEXT_N_EMPTY")));
	$row->AddField("TEXT_BOTTOM", (empty($arSeo['TEXT_BOTTOM'])?Loc::getMessage("TEXT_EMPTY"):Loc::getMessage("TEXT_N_EMPTY")));
}

$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$dbSeoList->SelectedRowsCount()), 
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"),
	"activate"=>GetMessage("KOKOC_LIST_ACTIVATE"),
	"deactivate"=>GetMessage("KOKOC_LIST_DEACTIVATE"),
));

$aButtons = array(
	array(
		"ICON" => "btn_new",
		"TEXT" => GetMessage("KOKOC_ADDEL_TEXT"),
		"LINK" => "/bitrix/admin/kokoc_seo_meta.php",
		"LINK_PARAM" => "",
		"TITLE" => GetMessage("KOKOC_ADDEL_TITLE")
	),
	array("SEPARATOR" => true),
	array(
		"ICON"=>"btn_copy",
		"TEXT"=>GetMessage("KOKOC_IMPORT_TEXT"),
		"TITLE"=>GetMessage("KOKOC_IMPORT_TITLE"),
		"ONCLICK"=>"jsUtils.OpenWindow('".CUtil::JSEscape(getLocalPath('modules/kokoc.seo/admin/import_csv.php'))."?table=meta', 1200, 500)",
	),
);
$lAdmin->AddAdminContextMenu($aButtons);

$lAdmin->CheckListMode();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage("FILTER_H_ID"),
		GetMessage("FILTER_H_URL"),
		GetMessage("FILTER_H_ACTIVE"),
		GetMessage("FILTER_H_CANONICAL"),
	)
);?>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?$oFilter->Begin();?>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_id_from_Change()
				{
					if(document.find_form.filter_id_to.value.length<=0)
					{
						document.find_form.filter_id_to.value = document.find_form.filter_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("FILTER_ID_FROM");?>
			<input type="text" name="filter_id_from" OnChange="filter_id_from_Change()" value="<?echo (IntVal($filter_id_from)>0)?IntVal($filter_id_from):""?>" size="10">
			<?echo Loc::getMessage("FILTER_ID_TO");?>
			<input type="text" name="filter_id_to" value="<?echo (IntVal($filter_id_to)>0)?IntVal($filter_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_URL");?>:</td>
		<td>
			<input type="text" name="filter_url" value="<?echo (strlen($filter_url)>0)?$filter_url:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ACTIVE")?>:</td>
		<td>
			<select name="filter_active">
				<option value=""><?echo Loc::getMessage("FILTER_ACTIVE_ALL")?></option>
				<option value="Y"<?if($filter_active=="Y") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_YES")?></option>
				<option value="N"<?if($filter_active=="N") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_NO")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_CANONICAL");?>:</td>
		<td>
			<input type="text" name="filter_canonical" value="<?echo (strlen($filter_canonical)>0)?$filter_canonical:""?>" size="10">
		</td>
	</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<?$lAdmin->DisplayList();?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
