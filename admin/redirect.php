<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\RedirectTable as RT;

$saleModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($saleModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

$ID = intVal($ID);

$back_url = '/bitrix/admin/kokoc_seo_redirects.php';

if($REQUEST_METHOD=="POST" && ($save!="" || $apply!="") && $saleModulePermissions=="W" && check_bitrix_sessid())
{
	if (empty($URL)){
		$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
	} else {
		$arFields = array(
			'ACTIVE'=>$ACTIVE,
			'URL'=>$URL,
			'NEW_URL'=>$NEW_URL,
			'TYPE'=>$TYPE,
			'IB_ID'=>$IB_ID,
			'IB_XML_ID'=>$IB_XML_ID,
			'IB_PAR_ID'=>$IB_PAR_ID,
		);

		if ($ID > 0){
			$result = RT::getByID($ID);
			if(!($redirect = $result->fetch())){
				$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
			} else {
				RT::update($redirect['ID'], $arFields);
			}
		} else {
			$res = RT::add($arFields);
			if ($res->isSuccess(true))
				$ID = $res->getId();
			else
				$strWarning = implode('; ', $res->getErrorMessages());
		}
		if (!$strWarning) {
			RT::getEntity()->cleanCache();
			if ($apply!="") {
				header('Location: http://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/kokoc_seo_redirect.php?ID='.$ID, true, 301);
			} elseif ($save!="") {
				header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
			}
			exit();
		}
	}
}
if ($GET['action'] == 'delete' && ($ID > 0) && $saleModulePermissions=="W" && check_bitrix_sessid()) {
	RT::delete($ID);
	RT::getEntity()->cleanCache();
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
	exit();
}

$aTabs = array();
$aTabs[] = array(
	"DIV" => "edit",
	"TAB" =>  Loc::getMessage("SEO_T_TAB1"),
	"ICON" => "score_type",
	"TITLE" => Loc::getMessage("SEO_T_TAB1_T"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

if ($ID > 0){
	$result = RT::getByID($ID);
	if(!($redirect = $result->fetch()))
		$ID='';
}

$aMenu = array(
	array(
		"TEXT" => GetMessage("SEO_T_LIST"),
		"TITLE" => GetMessage("SEO_T_LIST_TITLE"),
		"LINK" => "kokoc_seo_redirects.php",
		"ICON" => "btn_list"
	)
);

if(strlen($ID)>0)
{
	$aMenu[] = array("SEPARATOR"=>"Y");
	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_CREATE"),
		"TITLE" => GetMessage("SEO_T_CREATE_TITLE"),
		"LINK" => "kokoc_seo_redirects.php",
		"ICON" => "btn_new"
	);

	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_DEL"),
		"TITLE" => GetMessage("SEO_T_DEL_TITLE"),
		"LINK" => "javascript:if(confirm('".GetMessageJS("SEO_T_DEL_CONF")."')) window.location='/bitrix/admin/kokoc_seo_redirect.php?ID=".$ID."&action=delete&".bitrix_sessid_get()."';",
		"ICON" => "btn_delete"
		);
}

$context = new CAdminContextMenu($aMenu);
$context->Show();
?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form method="POST" id="form" name="form" action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="ID" value="<?echo $ID?>">
<?if(strlen($back_url)>0):?><input type="hidden" name="back_url" value="<?=htmlspecialcharsbx($back_url)?>"><?endif?>
<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
?>
	<?if(intVal($ID)>0):?>
	<tr>
		<td><?echo GetMessage("SEO_T_ID")?></td>
		<td><?=$ID?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><?echo GetMessage("SEO_T_ACTIVE")?></td>
		<td><input type="checkbox" name="ACTIVE" value="Y"<?if(($redirect['ACTIVE'] == "Y")||(intVal($ID)<1)) echo " checked"?>></td>
	</tr>
	<tr class="adm-detail-required-field">
		<td><?echo GetMessage("SEO_T_URL")?>:</td>
		<td><input type="text" name="URL" size="200"  maxlength="255" value="<?=($redirect['URL']?$redirect['URL']:$URL)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_NEW_URL")?>:</td>
		<td><input type="text" name="NEW_URL" size="200"  maxlength="255" value="<?=($redirect['NEW_URL']?$redirect['NEW_URL']:$NEW_URL)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TYPE")?>:</td>
		<td>
			<select name="TYPE">
				<option value="D"<?=((($redirect['TYPE']=='D')||($TYPE=='D')) ? ' selected':'')?>><?echo GetMessage("SEO_T_TYPE_D")?></option>
				<option value="E"<?=((($redirect['TYPE']=='E')||($TYPE=='E')) ? ' selected':'')?>><?echo GetMessage("SEO_T_TYPE_E")?></option>
				<option value="S"<?=((($redirect['TYPE']=='S')||($TYPE=='S')) ? ' selected':'')?>><?echo GetMessage("SEO_T_TYPE_S")?></option>
				<option value="X"<?=((($redirect['TYPE']=='X')||($TYPE=='X')) ? ' selected':'')?>><?echo GetMessage("SEO_T_TYPE_X")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_IB_ID")?>:</td>
		<td><input type="text" name="IB_ID" size="40"  maxlength="255" value="<?=($redirect['IB_ID']?$redirect['IB_ID']:$IB_ID)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_IB_XML_ID")?>:</td>
		<td><input type="text" name="IB_XML_ID" size="40"  maxlength="255" value="<?=($redirect['IB_XML_ID']?$redirect['IB_XML_ID']:$IB_XML_ID)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_IB_PAR_ID")?>:</td>
		<td><input type="text" name="IB_PAR_ID" size="40"  maxlength="255" value="<?=($redirect['IB_PAR_ID']?$redirect['IB_PAR_ID']:$IB_PAR_ID)?>"></td>
	</tr>
	<?if(intVal($ID)>0):?>
	<tr>
		<td><?echo GetMessage("SEO_T_COUNT")?>:</td>
		<td><?=$redirect['COUNT']?></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_LAST_USE")?>:</td>
		<td><?=$redirect['LAST_USE']?></td>
	</tr>
	<?endif;?>
<?
	$tabControl->Buttons(array("disabled"=>($saleModulePermissions<"W"), "back_url"=>$back_url));
	$tabControl->End();
?>
</form>


<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
