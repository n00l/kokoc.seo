<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\RedirectTable;

$ModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($ModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

if (COption::GetOptionString('kokoc.seo', 'use_redirects', false) !== 'Y')
	$strWarning = Loc::getMessage("KOKOC_SEO_REDIRECTS_OFF");

$sTableID = "kk_seo_redirects_list";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_id_from",
	"filter_id_to",
	"filter_date_from",
	"filter_date_to",
	"filter_type",
	"filter_active",
	"filter_url",
	"filter_new_url",
);

$lAdmin->InitFilter($arFilterFields);

$getListParams = array();
if(IntVal($filter_id_from)>0) $getListParams['filter'][">=ID"] = IntVal($filter_id_from);
if(IntVal($filter_id_to)>0) $getListParams['filter']["<=ID"] = IntVal($filter_id_to);
if(strlen($filter_date_from)>0) $getListParams['filter'][">=LAST_USE"] = trim($filter_date_from);
if(strlen($filter_date_to)>0)
{
	if($arDate = ParseDateTime($filter_date_to, CSite::GetDateFormat("FULL", SITE_ID)))
	{
		if(StrLen($filter_date_to) < 11)
		{
			$arDate["HH"] = 23;
			$arDate["MI"] = 59;
			$arDate["SS"] = 59;
		}

		$filter_date_to = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL", SITE_ID)), mktime($arDate["HH"], $arDate["MI"], $arDate["SS"], $arDate["MM"], $arDate["DD"], $arDate["YYYY"]));
		$getListParams['filter']["<=LAST_USE"] = $filter_date_to;
	}
	else
	{
		$filter_date_to = "";
	}
}
if(strlen($filter_type)>0) $getListParams['filter']["=TYPE"] = $filter_type;
if(strlen($filter_active)>0) $getListParams['filter']["=ACTIVE"] = ($filter_active == 'Y' ? 'Y' : 'N');
if(strlen($filter_url)>0) $getListParams['filter']["?URL"] = $filter_url;
if(strlen($filter_new_url)>0) $getListParams['filter']["?NEW_URL"] = $filter_new_url;

if($lAdmin->EditAction() && $ModulePermissions=="W")
{
	// TODO: add edit $FIELDS
}

if(($arID = $lAdmin->GroupAction()) && $ModulePermissions=="W")
{
	if ($_REQUEST['action'] == 'delete') {
		foreach ($arID as $id) {
			RedirectTable::delete($id);
		}
	} elseif ($_REQUEST['action'] == 'activate') {
		foreach ($arID as $id) {
			if (($row = RedirectTable::getRowById($id)) && ($row['ACTIVE'] == 'N'))
			{
				RedirectTable::update($id, array('ACTIVE' => 'Y'));
			}
		}
	} elseif ($_REQUEST['action'] == 'deactivate') {
		foreach ($arID as $id) {
			if (($row = RedirectTable::getRowById($id)) && ($row['ACTIVE'] == 'Y'))
			{
				RedirectTable::update($id, array('ACTIVE' => 'N'));
			}
		}
	}
	RedirectTable::getEntity()->cleanCache();
}

$lAdmin->AddHeaders(array(
	array("id"=>"ID",
		"content"=>"ID",
		"sort"=>"ID",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"ACTIVE",
		"content"=>Loc::getMessage("HEAD_ACTIVE"),
		"sort"=>"ACTIVE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"URL",
		"content"=>Loc::getMessage("HEAD_URL"),
		"sort"=>"URL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"NEW_URL",
		"content"=>Loc::getMessage("HEAD_NEW_URL"),
		"sort"=>"NEW_URL",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"TYPE",
		"content"=>Loc::getMessage("HEAD_TYPE"),
		"sort"=>"TYPE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"IB_ID",
		"content"=>Loc::getMessage("HEAD_IB_ID"),
		"sort"=>"IB_ID",
		"align"=>"right",
		"default"=>true,
	),
	array("id"=>"IB_XML_ID",
		"content"=>Loc::getMessage("HEAD_IB_XML_ID"),
		"sort"=>"IB_XML_ID",
		"align"=>"right",
		"default"=>true,
	),
	array("id"=>"IB_PAR_ID",
		"content"=>Loc::getMessage("HEAD_IB_PAR_ID"),
		"sort"=>"IB_PAR_ID",
		"align"=>"right",
		"default"=>true,
	),
	array("id"=>"COUNT",
		"content"=>Loc::getMessage("HEAD_COUNT"),
		"sort"=>"COUNT",
		"align"=>"right",
		"default"=>true,
	),
	array("id"=>"LAST_USE",
		"content"=>Loc::getMessage("HEAD_LAST_USE"),
		"sort"=>"LAST_USE",
		"align"=>"right",
		"default"=>true,
	),
));

$getListParams['select'] = array('*');
$getListParams['order'] = array($lAdmin->sort->getField() => $lAdmin->sort->getOrder());

$dbSeoList = new CAdminResult(RedirectTable::getList($getListParams), $sTableID);

if ($usePageNavigation)
{
	$dbSeoList->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
	$dbSeoList->NavRecordCount = $totalCount;
	$dbSeoList->NavPageCount = $totalPages;
	$dbSeoList->NavPageNomer = $navyParams['PAGEN'];
	$dbSeoList->nSelectedCount = $totalCount;
}
else
{
	$dbSeoList->NavStart();
}
$lAdmin->NavText($dbSeoList->GetNavPrint(Loc::getMessage("SALE_PRLIST")));

while ($arSeo = $dbSeoList->NavNext())
{
	$row =& $lAdmin->AddRow($arSeo['ID'], $arSeo);

	$row->AddField("ID", '<a href="/bitrix/admin/kokoc_seo_redirect.php?ID='.$arSeo['ID'].'">'.$arSeo['ID'].'</a>');
	$row->AddField("ACTIVE", $arSeo['ACTIVE']);
	$row->AddField("URL", $arSeo['URL']);
	$row->AddField("NEW_URL", $arSeo['NEW_URL']);
	$row->AddField("TYPE", $arSeo['TYPE']);
	$row->AddField("IB_ID", $arSeo['IB_ID']);
	$row->AddField("IB_XML_ID", $arSeo['IB_XML_ID']);
	$row->AddField("IB_PAR_ID", $arSeo['IB_PAR_ID']);
	$row->AddField("COUNT", $arSeo['COUNT']);
	$row->AddField("LAST_USE", $arSeo['LAST_USE']);
}

$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$dbSeoList->SelectedRowsCount()), 
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"),
	"activate"=>GetMessage("KOKOC_LIST_ACTIVATE"),
	"deactivate"=>GetMessage("KOKOC_LIST_DEACTIVATE"),
));

$aButtons = array(
	array(
		"ICON" => "btn_new",
		"TEXT" => GetMessage("KOKOC_ADDEL_TEXT"),
		"LINK" => "/bitrix/admin/kokoc_seo_redirect.php",
		"LINK_PARAM" => "",
		"TITLE" => GetMessage("KOKOC_ADDEL_TITLE")
	),
	array("SEPARATOR" => true),
	array(
		"ICON"=>"btn_copy",
		"TEXT"=>GetMessage("KOKOC_IMPORT_TEXT"),
		"TITLE"=>GetMessage("KOKOC_IMPORT_TITLE"),
		"ONCLICK"=>"jsUtils.OpenWindow('".CUtil::JSEscape(getLocalPath('modules/kokoc.seo/admin/import_csv.php'))."?table=redirect', 1200, 500)",
	),
);
$lAdmin->AddAdminContextMenu($aButtons);

$lAdmin->CheckListMode();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage("FILTER_H_ID"),
		GetMessage("FILTER_H_URL"),
		GetMessage("FILTER_H_NEW_URL"),
		GetMessage("FILTER_H_TYPE"),
		GetMessage("FILTER_H_ACTIVE"),
		GetMessage("FILTER_H_DATE"),
	)
);?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?$oFilter->Begin();?>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_id_from_Change()
				{
					if(document.find_form.filter_id_to.value.length<=0)
					{
						document.find_form.filter_id_to.value = document.find_form.filter_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("FILTER_ID_FROM");?>
			<input type="text" name="filter_id_from" OnChange="filter_id_from_Change()" value="<?echo (IntVal($filter_id_from)>0)?IntVal($filter_id_from):""?>" size="10">
			<?echo Loc::getMessage("FILTER_ID_TO");?>
			<input type="text" name="filter_id_to" value="<?echo (IntVal($filter_id_to)>0)?IntVal($filter_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_URL");?>:</td>
		<td>
			<input type="text" name="filter_url" value="<?echo (strlen($filter_url)>0)?$filter_url:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_NEW_URL");?>:</td>
		<td>
			<input type="text" name="filter_new_url" value="<?echo (strlen($filter_new_url)>0)?$filter_new_url:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_TYPE")?>:</td>
		<td>
			<select name="filter_type">
				<option value=""><?echo Loc::getMessage("FILTER_TYPE_ALL")?></option>
				<option value="D"<?if($filter_type=="D") echo " selected"?>><?echo Loc::getMessage("FILTER_TYPE_D")?></option>
				<option value="E"<?if($filter_type=="E") echo " selected"?>><?echo Loc::getMessage("FILTER_TYPE_E")?></option>
				<option value="S"<?if($filter_type=="S") echo " selected"?>><?echo Loc::getMessage("FILTER_TYPE_S")?></option>
				<option value="X"<?if($filter_type=="X") echo " selected"?>><?echo Loc::getMessage("FILTER_TYPE_X")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ACTIVE")?>:</td>
		<td>
			<select name="filter_active">
				<option value=""><?echo Loc::getMessage("FILTER_ACTIVE_ALL")?></option>
				<option value="Y"<?if($filter_active=="Y") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_YES")?></option>
				<option value="N"<?if($filter_active=="N") echo " selected"?>><?echo Loc::getMessage("FILTER_ACTIVE_NO")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><b><?echo Loc::getMessage("FILTER_DATE");?>:</b></td>
		<td>
			<?echo CalendarPeriod("filter_date_from", $filter_date_from, "filter_date_to", $filter_date_to, "find_form", "Y")?>
		</td>
	</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?CAdminMessage::ShowOldStyleError($error);?>

<?$lAdmin->DisplayList();?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';