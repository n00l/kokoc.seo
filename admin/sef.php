<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\SefTable as ST;

$saleModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($saleModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

$ID = intVal($ID);

$back_url = '/bitrix/admin/kokoc_seo_sefs.php';

if($REQUEST_METHOD=="POST" && ($save!="" || $apply!="") && $saleModulePermissions=="W" && check_bitrix_sessid())
{
	if (empty($URL)||empty($NEW_URL)){
		$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
	} else {
		$arFields = array(
			'ACTIVE'=>$ACTIVE,
			'URL'=>$URL,
			'NEW_URL'=>$NEW_URL,
			'ADD_PARAMS'=>$ADD_PARAMS,
			'META_ID'=>$META_ID,
		);

		if ($ID > 0){
			$result = ST::getByID($ID);
			if(!($sef = $result->fetch())){
				$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
			} else {
				$ID = $sef['ID'];
				ST::update($ID, $arFields);
			}
		} else {
			$res = ST::add($arFields);
			if ($res->isSuccess(true))
				$ID = $res->getId();
		}
		if ($apply!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/kokoc_seo_sef.php?ID='.$ID, true, 301);
			exit();
		} elseif ($save!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
			exit();
		}
		return;
	}
}
if ($_GET['action'] == 'delete' && ($ID > 0) && $saleModulePermissions=="W" && check_bitrix_sessid()) {
	ST::delete($ID);
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
	exit();
}

$aTabs = array();
$aTabs[] = array(
	"DIV" => "edit",
	"TAB" =>  Loc::getMessage("SEO_T_TAB1"),
	"ICON" => "score_type",
	"TITLE" => Loc::getMessage("SEO_T_TAB1_T"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

if ($ID > 0){
	$result = ST::getList(array('filter' => array('ID' => $ID), 'select' => array('*','META')));
	if(!($sef = $result->fetch()))
		$ID='';
}

$aMenu = array(
	array(
		"TEXT" => Loc::getMessage("SEO_T_LIST"),
		"TITLE" => Loc::getMessage("SEO_T_LIST_TITLE"),
		"LINK" => "kokoc_seo_sefs.php",
		"ICON" => "btn_list"
	)
);

if(strlen($ID)>0)
{
	$aMenu[] = array("SEPARATOR"=>"Y");
	$aMenu[] = array(
		"TEXT" => Loc::getMessage("SEO_T_CREATE"),
		"TITLE" => Loc::getMessage("SEO_T_CREATE_TITLE"),
		"LINK" => "kokoc_seo_sefs.php",
		"ICON" => "btn_new"
	);

	$aMenu[] = array(
		"TEXT" => Loc::getMessage("SEO_T_DEL"),
		"TITLE" => Loc::getMessage("SEO_T_DEL_TITLE"),
		"LINK" => "javascript:if(confirm('".GetMessageJS("SEO_T_DEL_CONF")."')) window.location='/bitrix/admin/kokoc_seo_sef.php?ID=".$ID."&action=delete&".bitrix_sessid_get()."';",
		"ICON" => "btn_delete"
		);
}

$context = new CAdminContextMenu($aMenu);
$context->Show();
?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form method="POST" id="form" name="form" action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="ID" value="<?echo $ID?>">
<?if(strlen($back_url)>0):?><input type="hidden" name="back_url" value="<?=htmlspecialcharsbx($back_url)?>"><?endif?>
<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
?>
	<?if(intVal($ID)>0):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_ID")?></td>
		<td><?=$ID?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_ACTIVE")?></td>
		<td><input type="checkbox" name="ACTIVE" value="Y"<?if(($sef['ACTIVE'] == "Y")||(intVal($ID)<1)) echo " checked"?>></td>
	</tr>
	<tr class="adm-detail-required-field">
		<td><?echo Loc::getMessage("SEO_T_URL")?>:</td>
		<td><input type="text" name="URL" size="200"  maxlength="255" value="<?=($sef['URL']?$sef['URL']:$URL)?>"></td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_NEW_URL")?>:</td>
		<td><input type="text" name="NEW_URL" size="200"  maxlength="255" value="<?=($sef['NEW_URL']?$sef['NEW_URL']:$NEW_URL)?>"></td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_ADD_PARAMS")?>:</td>
		<td><input type="text" name="ADD_PARAMS" size="200"  maxlength="255" value="<?=($sef['ADD_PARAMS']?$sef['ADD_PARAMS']:$ADD_PARAMS)?>"></td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_ID")?>:</td>
		<td>
			<input type="text" id="META_ID" name="META_ID" size="40"  maxlength="255" value="<?=($sef['META_ID']?$sef['META_ID']:$META_ID)?>">
			<input value="<?echo Loc::getMessage("SEO_T_META_FIND")?>" onclick="jsUtils.OpenWindow('/bitrix/admin/kokoc_seo_meta_search.php', 1200, 700);" type="button">
			<input value="<?echo Loc::getMessage("SEO_T_META_ADD")?>" onclick="jsUtils.OpenWindow('/bitrix/admin/kokoc_seo_meta_add.php', 900, 700);" type="button">
		</td>
	</tr>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_H1'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_H1")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_H1']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_TITLE'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_TITLE")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_TITLE']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_DESCRIPTION'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_DESCRIPTION")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_DESCRIPTION']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_KEYWORDS'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_KEYWORDS")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_KEYWORDS']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_TEXT_TOP'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_TT")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_TEXT_TOP']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_TEXT_MIDDLE'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_TM")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_TEXT_MIDDLE']?></td>
	</tr>
	<?endif;?>
	<?if(!empty($sef['KOKOC_SEO_SEF_META_TEXT_BOTTOM'])):?>
	<tr>
		<td><?echo Loc::getMessage("SEO_T_META_TB")?>:</td>
		<td><?=$sef['KOKOC_SEO_SEF_META_TEXT_BOTTOM']?></td>
	</tr>
	<?endif;?>
<?
	$tabControl->Buttons(array("disabled"=>($saleModulePermissions<"W"), "back_url"=>$back_url));
	$tabControl->End();
?>
</form>


<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
