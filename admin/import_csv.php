<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc;

$saleModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($saleModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$strError  = Loc::getMessage('KOKOC_SEO_ERR_MODULE_CANT_LOAD');

if (!empty($_REQUEST['table'])) {
	$table = ucfirst(mb_strtolower(preg_replace('/[^_A-Za-z\d]+/u', '', $_REQUEST['table'])));
	$tClass = '\\Kokoc\\Seo\\'.$table.'Table';
	if (class_exists($tClass) && method_exists($tClass, 'getMap')) {
		$arFieldsMap = $arFieldsTitles = array();
		foreach ($tClass::getMap() as $id => $field) {
			$arFieldsTitles[$field->getTitle()] = $id;
			$arFieldsMap[$id] = array(
				'TITLE' => $field->getTitle(),
				'TYPE' => $field->getDataType()
			);
			if (method_exists($field, 'isPrimary')) {
				$arFieldsMap[$id]['PRIMARY'] = $field->isPrimary();
			}
			if (method_exists($field, 'isAutocomplete')) {
				$arFieldsMap[$id]['AUTO'] = $field->isAutocomplete();
			}
			if (method_exists($field, 'getValues')) {
				$arFieldsMap[$id]['VALUES'] = $field->getValues();
			}
		}
	} else {
		$strError = Loc::getMessage("KOKOC_SEO_ERR_INVALID_TABLE");
	}
} else {
	$strError = Loc::getMessage("KOKOC_SEO_ERR_TABLE_NOT_SET");
}

if ((strlen($strError) <= 0) && $_FILES['csv'] && check_bitrix_sessid()) {
	if (strtolower(GetFileExtension($_FILES['csv']['name'])) != "csv")
	{
		$strError .= Loc::getMessage("KOKOC_SEO_ERR_INVALID_FILE_EXT")."<br>";
	} else {
		$csvFile = new CCSVData();
		$csvFile->LoadFile($_FILES['csv']['tmp_name']);
		$csvFile->MoveFirst();
		//$csvFile->SetDelimiter('\t');
		if (($arHeaders = $csvFile->Fetch())) {
			foreach ($arHeaders as $i => $key){
				if (!array_key_exists($key, $arFieldsMap)){
					if (array_key_exists($key, $arFieldsTitles)) {
						$arHeaders[$i] =  $arFieldsTitles[$key];
					} else {
						$strError .= Loc::getMessage("KOKOC_SEO_ERR_INVALID_FILE_HEARER").": ".$key."<br>";
					}
				}
			}

			if (strlen($strError) <= 0) {
				foreach ($arFieldsMap as $id => $field) {
					if ($field['PRIMARY']) {
						$primaryKey = $id;
						break;
					}
				}
				while (($row = $csvFile->Fetch())) {
					$row = array_combine($arHeaders, $row);
					if (empty($row[$primaryKey])) {
						$result = $tClass::add($row);
						if ($result->isSuccess(true)){
							$importSuccess[] = '[' . implode(';', $row) . '] ' . $result->getId();
						} else {
							$importError[] = '[' . implode(';', $row) . '] ' . implode('; ', $result->getErrorMessages());
						}
					} else {
						$id = $row[$primaryKey];
						unset($row[$primaryKey]);
						$result = $tClass::update($id, $row);
						if ($result->isSuccess(true)){
							$importSuccess[] = '[' . implode(';', $row) . '] ' . $result->getId();
						} else {
							$importError[] = '[' . implode(';', $row) . '] ' . implode('; ', $result->getErrorMessages());
						}
					}
				}
			}
		} else {
			$strError .= Loc::getMessage("KOKOC_SEO_ERR_CANT_READ_FILE")."<br>";
		}
		$csvFile->CloseFile();
	}
}

$aTabs = array(
	array(
		"DIV" => "edit",
		"TAB" => $table,
		"ICON" => "seo",
		"TITLE" => Loc::getMessage("KOKOC_SEO_TAB_TEXT"),
	)
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php");
?>
<?CAdminMessage::ShowOldStyleError($strError);?>
<?if (!empty($importSuccess)) CAdminMessage::ShowNote(Loc::getMessage('KOKOC_SEO_IMPORT_SUC').count($importSuccess));?>
<?if (!empty($importError)) CAdminMessage::ShowMessage(implode('<br>', array_slice($importError, 0, 10)));?>
<form method="POST" id="form" name="form" action="<?echo $APPLICATION->GetCurPage()?>" enctype="multipart/form-data">
<?=bitrix_sessid_post()?>
<input type="hidden" name="table" value="<?=$table?>">
<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
?>
<?foreach ($arFieldsMap as $id => $field){?>
	<tr>
		<td><?echo $field['TITLE']?>:</td>
		<td><?echo $id?></td>
		<td><?echo $field['TYPE']?></td>
	<?if ($field['PRIMARY']){?>
		<td>primary</td>
	<?}elseif ($field['VALUES']){?>
		<td><?echo '"'.implode('", "', $field['VALUES']).'"'?></td>
	<?}?>
	</tr>
<?}?>
<?
	$tabControl->Buttons();
?>
	<input name="csv" type="file" required="required">
	<input type="submit" name="submit" value="<?echo Loc::getMessage("KOKOC_SEO_IMPORT_SUBMIT")?>">
<?
	$tabControl->End();
?>
</form>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");?>