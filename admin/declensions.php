<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\DeclensionTable;

$ModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($ModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

$sTableID = "kk_seo_declensions_list";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arFilterFields = array(
	"filter_id_from",
	"filter_id_to",
	"filter_target",
	"filter_gender",
	"filter_case",
	"filter_number",
	"filter_type",
	"filter_generate",
);

$lAdmin->InitFilter($arFilterFields);

$getListParams = array();
if(IntVal($filter_id_from)>0) $getListParams['filter'][">=ID"] = IntVal($filter_id_from);
if(IntVal($filter_id_to)>0) $getListParams['filter']["<=ID"] = IntVal($filter_id_to);
if(strlen($filter_target)>0) $getListParams['filter']["=TARGET"] = $filter_target;
if(strlen($filter_gender)>0) $getListParams['filter']["=GENDER"] = $filter_gender;
if(strlen($filter_case)>0) $getListParams['filter']["=CASE"] = $filter_case;
if(strlen($filter_number)>0) $getListParams['filter']["=NUMBER"] = $filter_number;
if(strlen($filter_type)>0) $getListParams['filter']["=TYPE"] = $filter_type;
if(strlen($filter_generate)>0) $getListParams['filter']["=GENERATE"] = $filter_generate;

if($lAdmin->EditAction() && $ModulePermissions=="W")
{
	// TODO: add edit
}

if(($arID = $lAdmin->GroupAction()) && $ModulePermissions=="W")
{
	if ($_REQUEST['action'] == 'delete') {
		foreach ($arID as $id) {
			DeclensionTable::delete($id);
		}
	} elseif ($_REQUEST['action'] == 'to_manual') {
		foreach ($arID as $id) {
			if (($row = DeclensionTable::getRowById($id)) && ($row['GENERATE'] != 'M'))
			{
				DeclensionTable::update($id, array('GENERATE' => 'M'));
			}
		}
	}
}

$lAdmin->AddHeaders(array(
	array("id"=>"ID",
		"content"=>"ID",
		"sort"=>"ID",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"TARGET",
		"content"=>Loc::getMessage("HEAD_TARGET"),
		"sort"=>"TARGET",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"VALUE",
		"content"=>Loc::getMessage("HEAD_VALUE"),
		"sort"=>"VALUE",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"GENDER",
		"content"=>Loc::getMessage("HEAD_GENDER"),
		"sort"=>"GENDER",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"CASE",
		"content"=>Loc::getMessage("HEAD_CASE"),
		"sort"=>"CASE",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"NUMBER",
		"content"=>Loc::getMessage("HEAD_NUMBER"),
		"sort"=>"NUMBER",
		"align"=>"left",
		"default"=>true,
	),
	array("id"=>"TYPE",
		"content"=>Loc::getMessage("HEAD_TYPE"),
		"sort"=>"TYPE",
		"align"=>"center",
		"default"=>true,
	),
	array("id"=>"GENERATE",
		"content"=>Loc::getMessage("HEAD_GENERATE"),
		"sort"=>"GENERATE",
		"align"=>"left",
		"default"=>true,
	),
));

$getListParams['select'] = ['*'];
$getListParams['order'] = [$lAdmin->sort->getField()=>$lAdmin->sort->getOrder()];

$dbSeoList = new CAdminResult(DeclensionTable::getList($getListParams), $sTableID);

if ($usePageNavigation)
{
	$dbSeoList->NavStart($getListParams['limit'], $navyParams['SHOW_ALL'], $navyParams['PAGEN']);
	$dbSeoList->NavRecordCount = $totalCount;
	$dbSeoList->NavPageCount = $totalPages;
	$dbSeoList->NavPageNomer = $navyParams['PAGEN'];
	$dbSeoList->nSelectedCount = $totalCount;
}
else
{
	$dbSeoList->NavStart();
}
$lAdmin->NavText($dbSeoList->GetNavPrint(Loc::getMessage("SALE_PRLIST")));

$arGenders = array(
	'A' => Loc::getMessage("GENDER_A"),
	'M' => Loc::getMessage("GENDER_M"),
	'W' => Loc::getMessage("GENDER_W"),
	'N' => Loc::getMessage("GENDER_N"),
);
$arNumbers = array(
	'S' => Loc::getMessage("NUMBER_S"),
	'P' => Loc::getMessage("NUMBER_P"),
);
$arGenerations = array(
	'A' => Loc::getMessage("GENERATE_A"),
	'M' => Loc::getMessage("GENERATE_M"),
);
while ($arSeo = $dbSeoList->NavNext())
{
	$row =& $lAdmin->AddRow($arSeo['ID'], $arSeo);

	$row->AddField("ID", '<a href="/bitrix/admin/kokoc_seo_declension.php?ID='.$arSeo['ID'].'">'.$arSeo['ID'].'</a>');
	$row->AddField("TARGET", $arSeo['TARGET']);
	if (strlen(($value = $arSeo['VALUE']))>30)
		$value = mb_substr($value,0,30).'...';
	$row->AddField("VALUE", $value);
	$row->AddField("GENDER", $arGenders[$arSeo['GENDER']]);
	$row->AddField("CASE", $arSeo['CASE']);
	$row->AddField("NUMBER", $arNumbers[$arSeo['NUMBER']]);
	$row->AddField("TYPE", $arSeo['TYPE']);
	$row->AddField("GENERATE", $arGenerations[$arSeo['GENERATE']]);
}

$lAdmin->AddFooter(
	array(
		array("title"=>Loc::GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$dbSeoList->SelectedRowsCount()), 
		array("counter"=>true, "title"=>Loc::GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddGroupActionTable(Array(
	"delete"=>Loc::GetMessage("MAIN_ADMIN_LIST_DELETE"),
	"to_manual"=>Loc::GetMessage("KOKOC_SEO_TO_MANUAL"),
));

$aButtons = array(
	array(
		"ICON" => "btn_new",
		"TEXT" => GetMessage("KOKOC_ADDEL_TEXT"),
		"LINK" => "/bitrix/admin/kokoc_seo_declension.php",
		"LINK_PARAM" => "",
		"TITLE" => GetMessage("KOKOC_ADDEL_TITLE")
	),
	array("SEPARATOR" => true),
	array(
		"ICON"=>"btn_copy",
		"TEXT"=>GetMessage("KOKOC_IMPORT_TEXT"),
		"TITLE"=>GetMessage("KOKOC_IMPORT_TITLE"),
		"ONCLICK"=>"jsUtils.OpenWindow('".CUtil::JSEscape(getLocalPath('modules/kokoc.seo/admin/import_csv.php'))."?table=declension', 1200, 500)",
	),
);
$lAdmin->AddAdminContextMenu($aButtons);

$lAdmin->CheckListMode();

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		Loc::GetMessage("FILTER_H_ID"),
		Loc::GetMessage("FILTER_H_TYPE"),
		Loc::GetMessage("FILTER_H_TARGET"),
		Loc::GetMessage("FILTER_H_GENDER"),
		Loc::GetMessage("FILTER_H_CASE"),
		Loc::GetMessage("FILTER_H_NUMBER"),
		Loc::GetMessage("FILTER_H_GENERATE"),
	)
);?>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?$oFilter->Begin();?>
	<tr>
		<td><?echo Loc::getMessage("FILTER_ID");?>:</td>
		<td>
			<script type="text/javascript">
				function filter_id_from_Change()
				{
					if(document.find_form.filter_id_to.value.length<=0)
					{
						document.find_form.filter_id_to.value = document.find_form.filter_id_from.value;
					}
				}
			</script>
			<?echo Loc::getMessage("FILTER_ID_FROM");?>
			<input type="text" name="filter_id_from" OnChange="filter_id_from_Change()" value="<?echo (IntVal($filter_id_from)>0)?IntVal($filter_id_from):""?>" size="10">
			<?echo Loc::getMessage("FILTER_ID_TO");?>
			<input type="text" name="filter_id_to" value="<?echo (IntVal($filter_id_to)>0)?IntVal($filter_id_to):""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_TARGET");?>:</td>
		<td>
			<input type="text" name="filter_target" value="<?echo (strlen($filter_target)>0)?$filter_target:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_GENDER")?>:</td>
		<td>
			<select name="filter_gender">
				<option value=""><?echo Loc::getMessage("FILTER_ALL")?></option>
				<option value="A"<?if($filter_gender=="A") echo " selected"?>><?echo Loc::getMessage("GENDER_A")?></option>
				<option value="M"<?if($filter_gender=="M") echo " selected"?>><?echo Loc::getMessage("GENDER_M")?></option>
				<option value="W"<?if($filter_gender=="W") echo " selected"?>><?echo Loc::getMessage("GENDER_W")?></option>
				<option value="N"<?if($filter_gender=="N") echo " selected"?>><?echo Loc::getMessage("GENDER_N")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_CASE");?>:</td>
		<td>
			<input type="text" name="filter_case" value="<?echo (strlen($filter_case)>0)?$filter_case:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_NUMBER")?>:</td>
		<td>
			<select name="filter_number">
				<option value=""><?echo Loc::getMessage("FILTER_ALL")?></option>
				<option value="S"<?if($filter_number=="S") echo " selected"?>><?echo Loc::getMessage("NUMBER_S")?></option>
				<option value="P"<?if($filter_number=="P") echo " selected"?>><?echo Loc::getMessage("NUMBER_P")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_TYPE")?>:</td>
		<td>
			<input type="text" name="filter_type" value="<?echo (strlen($filter_type)>0)?$filter_type:""?>" size="10">
		</td>
	</tr>
	<tr>
		<td><?echo Loc::getMessage("FILTER_GENERATE")?>:</td>
		<td>
			<select name="filter_generate">
				<option value=""><?echo Loc::getMessage("FILTER_ALL")?></option>
				<option value="A"<?if($filter_generate=="A") echo " selected"?>><?echo Loc::getMessage("GENERATE_A")?></option>
				<option value="M"<?if($filter_generate=="M") echo " selected"?>><?echo Loc::getMessage("GENERATE_M")?></option>
			</select>
		</td>
	</tr>
<?
$oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
$oFilter->End();
?>
</form>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<?$lAdmin->DisplayList();?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
