<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Application,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\DeclensionTable as DT;

$saleModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($saleModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

$ID = intVal($ID);

if(!$back_url)
	$back_url = '/bitrix/admin/kokoc_seo_declensions.php';

if($REQUEST_METHOD=="POST" && ($save!="" || $apply!="") && $saleModulePermissions=="W" && check_bitrix_sessid())
{
	$arFields = array(
		'TARGET'=>$TARGET,
		'VALUE'=>$VALUE,
		'GENDER'=>$GENDER,
		'CASE'=>$CASE,
		'NUMBER'=>$NUMBER,
		'TYPE'=>$TYPE,
		'GENERATE'=>$GENERATE,
	);

	if ($ID > 0){
		$result = DT::getByID($ID);
		if(!($declension = $result->fetch())){
			$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
		} else {
			$ID = $declension['ID'];
			DT::update($ID, $arFields);
		}
	} else {
		$res = DT::add($arFields);
		if ($res->isSuccess(true))
			$ID = $res->getId();
		else
			$strWarning = implode('; ', $res->getErrorMessages());
	}
	if (!$strWarning) {
		DT::getEntity()->cleanCache();
		Application::getInstance()->getTaggedCache()->clearByTag('kokoc.seo.redirect');
		if ($apply!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/kokoc_seo_declension.php?ID='.$ID, true, 301);
		} elseif ($save!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
		}
		exit();
	}
}
if ($GET['action'] == 'delete' && ($ID > 0) && $saleModulePermissions=="W" && check_bitrix_sessid()) {
	DT::delete($ID);
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
	exit();
}

$aTabs = array();
$aTabs[] = array(
	"DIV" => "edit",
	"TAB" =>  Loc::getMessage("SEO_T_TAB1"),
	"ICON" => "score_type",
	"TITLE" => Loc::getMessage("SEO_T_TAB1_T"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

if ($ID > 0){
	$result = DT::getByID($ID);
	if(!($declension = $result->fetch()))
		$ID='';
}

$aMenu = array(
	array(
		"TEXT" => GetMessage("SEO_T_LIST"),
		"TITLE" => GetMessage("SEO_T_LIST_TITLE"),
		"LINK" => "kokoc_seo_declensions.php",
		"ICON" => "btn_list"
	)
);

if(strlen($ID)>0)
{
	$aMenu[] = array("SEPARATOR"=>"Y");
	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_CREATE"),
		"TITLE" => GetMessage("SEO_T_CREATE_TITLE"),
		"LINK" => "kokoc_seo_declensions.php",
		"ICON" => "btn_new"
	);

	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_DEL"),
		"TITLE" => GetMessage("SEO_T_DEL_TITLE"),
		"LINK" => "javascript:if(confirm('".GetMessageJS("SEO_T_DEL_CONF")."')) window.location='/bitrix/admin/kokoc_seo_declension.php?ID=".$ID."&action=delete&".bitrix_sessid_get()."';",
		"ICON" => "btn_delete"
		);
}

$context = new CAdminContextMenu($aMenu);
$context->Show();
?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form method="POST" id="form" name="form" action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="ID" value="<?echo $ID?>">
<input type="hidden" name="GENERATE" value="M">
<?if(strlen($back_url)>0):?><input type="hidden" name="back_url" value="<?=htmlspecialcharsbx($back_url)?>"><?endif?>
<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
?>
	<?if(intVal($ID)>0):?>
	<tr>
		<td><?echo GetMessage("SEO_T_ID")?></td>
		<td><?=$ID?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><?echo GetMessage("SEO_T_TARGET")?>:</td>
		<td><input type="text" name="TARGET" size="225"  maxlength="255" value="<?=($declension['TARGET']?$declension['TARGET']:$TARGET)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_VALUE")?>:</td>
		<td><input type="text" name="VALUE" size="225"  maxlength="255" value="<?=($declension['VALUE']?$declension['VALUE']:$VALUE)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_CASE")?>:</td>
		<td><input type="text" name="CASE" size="20"  maxlength="255" value="<?=($declension['CASE']?$declension['CASE']:$CASE)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_GENDER")?>:</td>
		<td>
			<select name="GENDER">
				<option value="A"<?=((($redirect['GENDER']=='A')||($GENDER=='A')) ? ' selected':'')?>><?echo GetMessage("SEO_T_GENDER_A")?></option>
				<option value="M"<?=((($redirect['GENDER']=='M')||($GENDER=='M')) ? ' selected':'')?>><?echo GetMessage("SEO_T_GENDER_M")?></option>
				<option value="W"<?=((($redirect['GENDER']=='W')||($GENDER=='W')) ? ' selected':'')?>><?echo GetMessage("SEO_T_GENDER_W")?></option>
				<option value="N"<?=((($redirect['GENDER']=='N')||($GENDER=='N')) ? ' selected':'')?>><?echo GetMessage("SEO_T_GENDER_N")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_NUMBER")?>:</td>
		<td>
			<select name="NUMBER">
				<option value="S"<?=((($redirect['NUMBER']=='S')||($NUMBER=='S')) ? ' selected':'')?>><?echo GetMessage("SEO_T_NUMBER_S")?></option>
				<option value="P"<?=((($redirect['NUMBER']=='P')||($NUMBER=='P')) ? ' selected':'')?>><?echo GetMessage("SEO_T_NUMBER_P")?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TYPE")?>:</td>
		<td><input type="text" name="TYPE" size="20"  maxlength="255" value="<?=($declension['TYPE']?$declension['TYPE']:$TYPE)?>"></td>
	</tr>
<?
	$tabControl->Buttons(array("disabled"=>($saleModulePermissions<"W"), "back_url"=>$back_url));
	$tabControl->End();
?>
</form>
<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';