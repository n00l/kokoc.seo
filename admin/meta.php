<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Loader,
	Bitrix\Main\Localization\Loc,
	Kokoc\Seo\MetaTable as MT;

$saleModulePermissions = $APPLICATION->GetGroupRight("seo");// use same rights as seo

if($saleModulePermissions == "D")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loc::loadMessages(__FILE__);

if (!Loader::includeModule("kokoc.seo"))
	$error = Loc::getMessage('KOKOC_SEO_MODULE_CANT_LOAD');

$ID = intVal($ID);

if(!$back_url)
	$back_url = '/bitrix/admin/kokoc_seo_metas.php';

if($REQUEST_METHOD=="POST" && ($save!="" || $apply!="") && $saleModulePermissions=="W" && check_bitrix_sessid())
{
	$arFields = array(
		'ACTIVE'=>$ACTIVE,
		'URL'=>$URL,
		'H1'=>$H1,
		'TITLE'=>$TITLE,
		'DESCRIPTION'=>$DESCRIPTION,
		'KEYWORDS'=>$KEYWORDS,
		'CANONICAL'=>$CANONICAL,
		'TEXT_TOP'=>$TEXT_TOP,
		'TEXT_TOP_TYPE'=>$TEXT_TOP_TYPE,
		'TEXT_MIDDLE'=>$TEXT_MIDDLE,
		'TEXT_MIDDLE_TYPE'=>$TEXT_MIDDLE_TYPE,
		'TEXT_BOTTOM'=>$TEXT_BOTTOM,
		'TEXT_BOTTOM_TYPE'=>$TEXT_BOTTOM_TYPE,
	);

	if ($ID > 0){
		$result = MT::getByID($ID);
		if(!($meta = $result->fetch())){
			$strWarning = 'Error: '.Loc::getMessage("SEO_E_SAVE");
		} else {
			$ID = $meta['ID'];
			MT::update($ID, $arFields);
		}
	} else {
		$res = MT::add($arFields);
		if ($res->isSuccess(true))
			$ID = $res->getId();
		else
			$strWarning = implode('; ', $res->getErrorMessages());
	}
	if (!$strWarning) {
		if ($apply!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . '/bitrix/admin/kokoc_seo_meta.php?ID='.$ID, true, 301);
		} elseif ($save!="") {
			header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
		}
		exit();
	}
}
if ($GET['action'] == 'delete' && ($ID > 0) && $saleModulePermissions=="W" && check_bitrix_sessid()) {
	MT::delete($ID);
	header('Location: http://' . $_SERVER['HTTP_HOST'] . $back_url, true, 301);
	exit();
}

$aTabs = array();
$aTabs[] = array(
	"DIV" => "edit",
	"TAB" =>  Loc::getMessage("SEO_T_TAB1"),
	"ICON" => "score_type",
	"TITLE" => Loc::getMessage("SEO_T_TAB1_T"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

if ($ID > 0){
	$result = MT::getByID($ID);
	if(!($meta = $result->fetch()))
		$ID='';
}
$str_TEXT_TOP = $meta['TEXT_TOP']?$meta['TEXT_TOP']:$TEXT_TOP;
$str_TEXT_TOP_TYPE = $meta['TEXT_TOP_TYPE']?$meta['TEXT_TOP_TYPE']:$TEXT_TOP_TYPE;
$str_TEXT_MIDDLE = $meta['TEXT_MIDDLE']?$meta['TEXT_MIDDLE']:$TEXT_MIDDLE;
$str_TEXT_MIDDLE_TYPE = $meta['TEXT_MIDDLE_TYPE']?$meta['TEXT_MIDDLE_TYPE']:$TEXT_MIDDLE_TYPE;
$str_TEXT_BOTTOM = $meta['TEXT_BOTTOM']?$meta['TEXT_BOTTOM']:$TEXT_BOTTOM;
$str_TEXT_BOTTOM_TYPE = $meta['TEXT_BOTTOM_TYPE']?$meta['TEXT_BOTTOM_TYPE']:$TEXT_BOTTOM_TYPE;

$aMenu = array(
	array(
		"TEXT" => GetMessage("SEO_T_LIST"),
		"TITLE" => GetMessage("SEO_T_LIST_TITLE"),
		"LINK" => "kokoc_seo_metas.php",
		"ICON" => "btn_list"
	)
);

if(strlen($ID)>0)
{
	$aMenu[] = array("SEPARATOR"=>"Y");
	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_CREATE"),
		"TITLE" => GetMessage("SEO_T_CREATE_TITLE"),
		"LINK" => "kokoc_seo_metas.php",
		"ICON" => "btn_new"
	);

	$aMenu[] = array(
		"TEXT" => GetMessage("SEO_T_DEL"),
		"TITLE" => GetMessage("SEO_T_DEL_TITLE"),
		"LINK" => "javascript:if(confirm('".GetMessageJS("SEO_T_DEL_CONF")."')) window.location='/bitrix/admin/kokoc_seo_meta.php?ID=".$ID."&action=delete&".bitrix_sessid_get()."';",
		"ICON" => "btn_delete"
		);
}

$context = new CAdminContextMenu($aMenu);
$context->Show();
?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form method="POST" id="form" name="form" action="<?echo $APPLICATION->GetCurPage()?>">
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="ID" value="<?echo $ID?>">
<?if(strlen($back_url)>0):?><input type="hidden" name="back_url" value="<?=htmlspecialcharsbx($back_url)?>"><?endif?>
<?
	$tabControl->Begin();
	$tabControl->BeginNextTab();
?>
	<?if(intVal($ID)>0):?>
	<tr>
		<td><?echo GetMessage("SEO_T_ID")?></td>
		<td><?=$ID?></td>
	</tr>
	<?endif;?>
	<tr>
		<td><?echo GetMessage("SEO_T_ACTIVE")?></td>
		<td><input type="checkbox" name="ACTIVE" value="Y"<?if(($meta['ACTIVE'] == "Y")||(intVal($ID)<1)) echo " checked"?>></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_URL")?>:</td>
		<td><input type="text" name="URL" size="225"  maxlength="255" value="<?=($meta['URL']?$meta['URL']:$URL)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_H1")?>:</td>
		<td><input type="text" name="H1" size="225"  maxlength="255" value="<?=($meta['H1']?$meta['H1']:$H1)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TITLE")?>:</td>
		<td><input type="text" name="TITLE" size="225"  maxlength="255" value="<?=($meta['TITLE']?$meta['TITLE']:$TITLE)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_DESCRIPTION")?>:</td>
		<td><input type="text" name="DESCRIPTION" size="225"  maxlength="255" value="<?=($meta['DESCRIPTION']?$meta['DESCRIPTION']:$DESCRIPTION)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_KEYWORDS")?>:</td>
		<td><input type="text" name="KEYWORDS" size="225"  maxlength="255" value="<?=($meta['KEYWORDS']?$meta['KEYWORDS']:$KEYWORDS)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_CANONICAL")?>:</td>
		<td><input type="text" name="CANONICAL" size="225"  maxlength="255" value="<?=($meta['CANONICAL']?$meta['CANONICAL']:$CANONICAL)?>"></td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TEXT_TOP_TYPE")?></td>
		<td>
			<input type="radio" name="TEXT_TOP_TYPE" id="TEXT_TOP_TYPE_text" value="TEXT"<?if($str_TEXT_TOP_TYPE!="HTML")echo " checked"?>>
			<label for="TEXT_TOP_TYPE_text"><?echo GetMessage("SEO_T_TEXT_TYPE_TEXT")?></label> /
			<input type="radio" name="TEXT_TOP_TYPE" id="TEXT_TOP_TYPE_html" value="HTML"<?if($str_TEXT_TOP_TYPE=="HTML")echo " checked"?>>
			<label for="TEXT_TOP_TYPE_html"><?echo GetMessage("SEO_T_TEXT_TYPE_HTML")?></label>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<textarea cols="60" rows="15"  name="TEXT_TOP" style="width:100%"><?echo $str_TEXT_TOP?></textarea>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TEXT_MIDDLE_TYPE")?></td>
		<td>
			<input type="radio" name="TEXT_MIDDLE_TYPE" id="TEXT_MIDDLE_TYPE_text" value="TEXT"<?if($str_TEXT_MIDDLE_TYPE!="HTML")echo " checked"?>>
			<label for="TEXT_MIDDLE_TYPE_text"><?echo GetMessage("SEO_T_TEXT_TYPE_TEXT")?></label> /
			<input type="radio" name="TEXT_MIDDLE_TYPE" id="TEXT_MIDDLE_TYPE_html" value="HTML"<?if($str_TEXT_MIDDLE_TYPE=="HTML")echo " checked"?>>
			<label for="TEXT_MIDDLE_TYPE_html"><?echo GetMessage("SEO_T_TEXT_TYPE_HTML")?></label>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<textarea cols="60" rows="15"  name="TEXT_MIDDLE" style="width:100%"><?echo $str_TEXT_MIDDLE?></textarea>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("SEO_T_TEXT_BOTTOM_TYPE")?></td>
		<td>
			<input type="radio" name="TEXT_BOTTOM_TYPE" id="TEXT_BOTTOM_TYPE_text" value="TEXT"<?if($str_TEXT_BOTTOM_TYPE!="HTML")echo " checked"?>>
			<label for="TEXT_BOTTOM_TYPE_text"><?echo GetMessage("SEO_T_TEXT_TYPE_TEXT")?></label> /
			<input type="radio" name="TEXT_BOTTOM_TYPE" id="TEXT_BOTTOM_TYPE_html" value="HTML"<?if($str_TEXT_BOTTOM_TYPE=="HTML")echo " checked"?>>
			<label for="TEXT_BOTTOM_TYPE_html"><?echo GetMessage("SEO_T_TEXT_TYPE_HTML")?></label>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<textarea cols="60" rows="15"  name="TEXT_BOTTOM" style="width:100%"><?echo $str_TEXT_BOTTOM?></textarea>
		</td>
	</tr>
<?
	$tabControl->Buttons(array("disabled"=>($saleModulePermissions<"W"), "back_url"=>$back_url));
	$tabControl->End();
?>
</form>


<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';
